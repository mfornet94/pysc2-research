import numpy as np


class Buffer:
    """ Cyclic experience replay buffer
    """

    def __init__(self, config):
        self.capacity = config['buffer']['capacity']
        self.data = [None] * self.capacity
        self.size = 0
        self.pointer = 0

    def add_one(self, experience):
        self.data[self.pointer] = experience
        self.pointer += 1

        self.size = min(self.size + 1, self.capacity)

        if self.pointer == self.capacity:
            self.pointer = 0

    def add(self, experiences):
        for e in experiences:
            self.add_one(e)

    def sample(self, size):
        size = min(size, self.size)
        index = np.random.choice(self.size, size=size, replace=False)
        return [self.data[idx] for idx in index]
