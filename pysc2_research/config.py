class Configuration:
    # Setup name. Used as name for models and logs folder.
    name = "MoveToBeaconSimple"

    # Initialize SC2Env
    map_name = "MoveToBeacon"
    step_mul = 16  # Take care in the future with this parameter
    visualize = False

    # Training
    step_per_episode = 120  # Number of steps per episode
    epochs = 10000  # Number of iterations to sample and train policies
    episodes = 4  # Number of episodes sampled in each epoch
    training_steps = 256  # Number of steps training policy on minibatch
    batch_size = 128  # Minibatch size for training
    train_verbose = False

    # HER
    her = True
    her_new_goals = 25

    # Checkpoints
    train_from_scratch = True
    saved_models_path = 'saved_models'

    # Logging
    tb_logs_path = 'tb_logs'

    # Buffer
    buffer_size = 16384  # Size of the Buffer Experience

    # Testing
    testing_episodes = 4  # Number of episodes used to test the success rate of the policy 128

    # QLearning | This parameters were selected empirically without any fine tuning
    discount = .9
    epsilon = .1
    alpha = .7
