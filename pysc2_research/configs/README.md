# Configuration formatting

name:
    Name of this configuration

pysc2:
    Boolean. Whether to use pysc2 minigame or not

pysc2_args:
    Arguments passed to load PySc2Minigame
    + map_name
    + screen_resolution (124)
    + minimap_resolution (84)
    + visualize
        Boolean
    + step_mul
        Frequency of actions. Every action will be done every `step_mul` frames

environment:
    + name:
    + render:
        Boolean:
    + args
    + kwargs
        Parameters to start environment

controller:
    + name
    + kwargs:
        Control parameters for the controller

rl:
    Arguments to load Reinforcement learning algorithm

    model

    buffer:
        Parameters given to buffer

        capacity:

    her:
        her_density:
            Percent of samples in the minibatch to upgrade with her strategy.

    actor_critic

    train:
        episodes:
        batch_size:

tensorboard:
    path

verbose:
    Print logs to stdout