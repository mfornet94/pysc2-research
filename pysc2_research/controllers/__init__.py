from tensorboardX import SummaryWriter

from controllers.toy_controller import ToyController


def build_controller(tbx: SummaryWriter, config):
    if config['controller']['name'] == 'ToyController':
        return ToyController(tbx, config)

    else:
        raise ValueError()
