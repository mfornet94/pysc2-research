from tensorboardX import SummaryWriter


class Controller:
    def __init__(self, tbx: SummaryWriter, config):
        """
        Initialize controller. Build and load everything.

        :param tbx: SummaryWriter to log data while training/testing.
        :param config: Arguments to feed controller.
        """
        self.tbx = tbx
        self.config = config

    def summary(self):
        """
            Summary about this controller.
            Description about the behaviour and details about the inner structure.

            Show to stdout.
        """
        raise NotImplementedError()

    def select_action(self, state):
        """
        Select action according to current state.
        """
        raise NotImplementedError()

    def train(self, minibatch, history):
        """
        Train on minibatch

        :param minibatch: Minibatch of experience to train
        :param history: Log values relevant to training.
        """
        raise NotImplementedError()
