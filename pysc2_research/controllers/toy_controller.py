import keras.backend as K
import numpy as np
from keras import Sequential, Input, Model
from keras.layers import Dense, Lambda, Reshape, Concatenate, Add
from keras.optimizers import RMSprop
from keras.regularizers import l2
from keras.utils import to_categorical

from controllers.base import Controller
from utils import LogSchedule

OPTIMIZER = RMSprop


def cross_entropy(y_true, y_pred):
    return -y_true * K.log(y_pred)


def maximize(y_true, y_pred):
    return -y_true * y_pred


class Actor:
    def __init__(self, config):
        self.config = config
        self.lr = config['actor_lr']
        self.n = config['n']
        self._build()

    def _build(self):
        state = Input(shape=(4,))
        action = Input(shape=(2, self.n))

        reg = l2(self.config["regularization"])

        hidden = Dense(100, activation='relu', kernel_regularizer=reg, bias_regularizer=reg)(state)
        x_proba = Dense(self.n, activation='softmax', kernel_regularizer=reg, bias_regularizer=reg)(hidden)
        y_proba = Dense(self.n, activation='softmax', kernel_regularizer=reg, bias_regularizer=reg)(hidden)

        action_proba = Lambda(self._build_proba)([x_proba, y_proba, action])

        entropy = self._build_entropy(x_proba, y_proba)

        self.policy_model = Model(inputs=state, outputs=[x_proba, y_proba])
        self.proba_model = Model(inputs=[state, action], outputs=[action_proba, entropy])

        self.opt = OPTIMIZER(lr=self.lr)
        self.proba_model.compile(optimizer=self.opt, loss=[cross_entropy, maximize])

    def _build_proba(self, data):
        x_proba, y_proba, categ_action = data
        x_proba = Reshape((1, self.n))(x_proba)
        y_proba = Reshape((1, self.n))(y_proba)
        coord_proba = Concatenate(1)([x_proba, y_proba])
        axis_prob = K.sum(categ_action * coord_proba, 2)
        return K.prod(axis_prob, 1, keepdims=True) + 1e-10

    def _build_entropy(self, x_proba, y_proba):
        x_entropy = Lambda(self._entropy)(x_proba)
        y_entropy = Lambda(self._entropy)(y_proba)
        return Add()([x_entropy, y_entropy])

    @staticmethod
    def _entropy(dist):
        return K.sum(-dist * K.log(dist + 1e-10), -1, keepdims=True)

    def choose_action(self, s):
        if self.config['optimal_policy']:
            x, y = s.flatten()[-2:]
            return int(x), int(y)
        else:
            x_proba, y_proba = self.policy_model.predict(s)

            x = np.random.choice(self.n, p=x_proba.flatten())
            y = np.random.choice(self.n, p=y_proba.flatten())
            return x, y

    def learn(self, state, action, td_error, entropy_mult, history):
        action = to_categorical(action, self.n)
        _, entropy = self.proba_model.predict([state, action])
        history.add_scalar("actor/entropy", np.mean(entropy))

        entropy_mult = np.full((state.shape[0],), entropy_mult)
        self.proba_model.train_on_batch([state, action], [td_error, entropy_mult])

    def save(self, path):
        self.proba_model.save_weights(path)

    def load(self, path):
        self.proba_model.load_weights(path)

    def get_weights(self):
        return self.proba_model.get_weights()

    def set_weights(self, weights):
        self.proba_model.set_weights(weights)


class Critic:
    def __init__(self, config):
        self.config = config
        self.model = Sequential()
        self.model.add(Dense(20, activation='relu', input_shape=(4,)))
        # self.model.add(Dense(20, activation='relu'))
        self.model.add(Dense(1))
        self.opt = OPTIMIZER(lr=config['critic_lr'])
        self.model.compile(optimizer=self.opt, loss='mse')

    def get_error(self, s, r, s_):
        vs = self.model.predict(s)
        vs_ = self.model.predict(s_)
        t = r + self.config['discount'] * vs_
        td_error = t - vs
        return td_error

    def learn(self, s, r, s_):
        vs_ = self.model.predict(s_)
        t = r + self.config['discount'] * vs_
        self.model.train_on_batch(s.reshape(-1, 4), t)

    def save(self, path):
        self.model.save_weights(path)

    def get_weights(self):
        return self.model.get_weights()

    def set_weights(self, weights):
        self.model.set_weights(weights)


def tonp(data):
    return np.array(data).reshape(1, -1)


class SmoothValue:
    def __init__(self):
        self._value = None

    def update(self, x):
        if self._value is None:
            self._value = x
        else:
            self._value = .99 * self._value + .01 * x

    @property
    def value(self):
        return self._value


class Brain:
    def __init__(self, config):
        self.config = config

        self.actor = Actor(config)
        self.critic = Critic(config)

        self.actor_target = Actor(config)
        self.actor_target.set_weights(self.actor.get_weights())

        self.critic_target = Critic(config)
        self.critic_target.set_weights(self.critic.get_weights())

        self.entropy_schedule = LogSchedule(config['entropy_init'], config['entropy_end'], config['entropy_time'])

        self.steps = 0

    def select_action(self, state):
        return self.actor_target.choose_action(state)

    def critic_learn(self, state, reward, next_state, history):
        self.steps += 1

        vs = self.critic.model.predict(state)
        vs_ = self.critic_target.model.predict(next_state)

        history.add_scalar("value/critic", np.mean(vs))
        history.add_scalar("value/target_critic", np.mean(vs_))

        t = reward + self.config['discount'] * vs_
        td_error = t - vs

        self.critic.model.train_on_batch(state.reshape(-1, 4), t)

        if self.config["continuous_update"]:
            critic_weights = self.critic.get_weights()
            critic_target_weights = self.critic_target.get_weights()

            params = []
            for wa, wat in zip(critic_weights, critic_target_weights):
                nw = self.config['alpha_critic'] * wa + (1. - self.config['alpha_critic']) * wat
                params.append(nw)

            self.critic_target.set_weights(params)

        else:
            if self.steps % self.config['update_critic_every'] == 0:
                self.critic_target.set_weights(self.critic.get_weights())

        return td_error

    def actor_learn(self, state, action, td_error, history):
        self.actor.learn(state, action, td_error, self.entropy_schedule.value(self.steps), history)

        if self.config['continuous_update']:
            actor_weights = self.actor.get_weights()
            actor_target_weights = self.actor_target.get_weights()

            params = []
            for wa, wat in zip(actor_weights, actor_target_weights):
                nw = self.config['alpha_actor'] * wa + (1. - self.config['alpha_actor']) * wat
                params.append(nw)

            self.actor_target.set_weights(params)
        else:
            if self.steps % self.config['update_actor_every'] == 0:
                self.actor_target.set_weights(self.actor.get_weights())


class ToyController(Controller):
    def __init__(self, *args):
        super().__init__(*args)

        self.brain = Brain(self.config['controller']['kwargs'])

    def summary(self):
        self.brain.actor.proba_model.summary()
        self.brain.critic.model.summary()

    @staticmethod
    def _fix_state(state):
        return np.array([[state.state[0], state.state[1], state.goal[0], state.goal[1]]])

    @staticmethod
    def _fix(minibatch):
        state, action, reward, next_state = [], [], [], []

        for s, a, r, ns, _ in minibatch:
            state.append(ToyController._fix_state(s).flatten())
            action.append(list(a))
            reward.append([r])
            next_state.append(ToyController._fix_state(ns).flatten())

        return list(map(np.array, [state, action, reward, next_state]))

    def select_action(self, state):
        state = self._fix_state(state)
        return self.brain.select_action(state)

    def train(self, minibatch, history=None):
        s, a, r, ns = self._fix(minibatch)

        td_error = self.brain.critic_learn(s, r, ns, history)
        history.add_scalar("train/td_error", np.mean(td_error))
        self.brain.actor_learn(s, a, td_error, history)
