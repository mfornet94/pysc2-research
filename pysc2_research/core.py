import time
from datetime import datetime
from os.path import join

from pysc2.env import sc2_env
from pysc2.lib import features
from tensorboardX import SummaryWriter

from buffer import Buffer
from controllers import build_controller
from controllers.base import Controller
from environments import build_sc2_env, build_env
from environments.base import Environment
from her import expand_experience
from history import History
from utils import SmoothValue


def run(config):
    tbx = SummaryWriter(log_dir=join(config['tensorboard']['path'], '{}-{}'.format(config['name'], datetime.now())))
    history = History(tbx)

    if config['pysc2']:
        pysc2_args = config['pysc2_args']

        deep_env = sc2_env.SC2Env(
            map_name=pysc2_args['map_name'],
            agent_interface_format=features.AgentInterfaceFormat(
                feature_dimensions=features.Dimensions(screen=pysc2_args['screen_resolution'],
                                                       minimap=pysc2_args['minimap_resolution']),
                use_feature_units=True),
            step_mul=pysc2_args['step_mul'],
            game_steps_per_episode=0,
            visualize=pysc2_args['visualize'])

        env: Environment = build_sc2_env(deep_env, config)
    else:
        env = build_env(config)

    controller: Controller = build_controller(tbx, config)

    if config['verbose']:
        controller.summary()

    buffer = Buffer(config)
    total_steps = 0

    reward_acc = SmoothValue()

    start = time.time()

    for episode in range(1, config['rl']['train']['episodes'] + 1):
        total_reward = 0
        steps = 0

        done = False

        state = env.reset()

        while not done:
            steps += 1
            total_steps += 1

            action = controller.select_action(state)
            new_state, reward, done, info = env.step(action)

            buffer.add_one((state, action, reward, new_state, info))

            state = new_state
            total_reward += reward

            minibatch = buffer.sample(config['rl']['train']['batch_size'])
            minibatch = expand_experience(minibatch, config, env)
            controller.train(minibatch, history)

        reward_acc.update(total_reward)

        episode_per_second = (time.time() - start) / episode

        history.add_scalar("train/reward", total_reward)
        history.add_scalar("train/episodes_per_second", episode_per_second)
        history.add_scalar("buffer/size", buffer.size)

        if config['verbose']:
            print("total reward: {} steps: {} total steps: {}".format(
                reward_acc.value, steps, total_steps
            ))

    return history
