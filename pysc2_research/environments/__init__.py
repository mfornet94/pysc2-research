from .grid_param import GridParam


def build_env(config):
    if config['environment']['name'] == 'GridParam':
        return GridParam(**config['environment']['kwargs'])
    else:
        raise ValueError("Environment Not Found")


def build_sc2_env(deep_env, config):
    pass
