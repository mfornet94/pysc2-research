from gym import Env
from pysc2.env.sc2_env import SC2Env


class Environment(Env):
    def render(self, mode='human'):
        pass

    def reset(self):
        raise NotImplementedError()

    def step(self, action):
        raise NotImplementedError()

    @staticmethod
    def is_good_experience(experience):
        """
        While adding experience to HER ignore experience that harm performance.
        """
        return True

    @staticmethod
    def update_state(state, new_goal):
        """
        Replace in old_state the old_goal to new_goal
        """
        raise NotImplementedError()

    @staticmethod
    def calculate_reward(state, action, new_state, info):
        """
        Calculate the new reward
        """
        raise NotImplementedError()

    @staticmethod
    def state_to_goal(state):
        """
        return the local goal of state
        """
        raise NotImplementedError()


class SC2Environment(Environment):
    def __init__(self, sc2env: SC2Env):
        self.sc2env = sc2env
        self.obs = None
        self._deep_obs = None

        self.episode_len = None
        self.episode_reward = None

    def reset(self):
        self._deep_obs = self.sc2env.reset()
        self.obs = self.build_obs()

        self.episode_len = 0
        self.episode_reward = 0

        return self.obs

    def _step(self, action):
        """ This function should build real step on sc2env
        """
        raise NotImplementedError()

    def step(self, action):
        action = self._step(action)
        self._deep_obs = self.sc2env.step([action])
        self.obs = self.build_obs()

        self.episode_len += 1
        self.episode_reward += self.deep_obs.reward

        done = self.deep_obs.last()

        info = {
            "episode": {
                "l": self.episode_len,
                "r": self.episode_reward
            }
        }

        return self.obs, self.deep_obs.reward, done, info

    @property
    def deep_obs(self):
        return self._deep_obs[0]

    def build_obs(self):
        raise NotImplementedError()

    @staticmethod
    def update_state(state, new_goal):
        raise NotImplementedError()

    @staticmethod
    def calculate_reward(state, action, new_state, info):
        raise NotImplementedError()

    @staticmethod
    def state_to_goal(state):
        raise NotImplementedError()
