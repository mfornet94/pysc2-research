import random
from collections import OrderedDict
from copy import deepcopy

import gym
import numpy as np

from .base import Environment


def clamp(x, min_x, max_x):
    return min(max(x, min_x), max_x)


class GridParam(Environment):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 50
    }

    max_steps = 120
    multi_step = 3
    dx = [0, 1, 0, -1]
    dy = [1, 0, -1, 0]

    def __init__(self, n):
        self.n = n

        self.rnd = random.Random(None)

        self.steps = None
        self.position = None
        self.goal = None

        self.viewer = None

        # Observation space
        # self.observation_space = gym.spaces.Dict({
        #     "state": gym.spaces.MultiDiscrete([n, n]),
        #     "goal": gym.spaces.MultiDiscrete([n, n])
        # })
        self.observation_space = gym.spaces.MultiDiscrete([n, n, n, n])

        # Action space
        # self.action_space = gym.spaces.Box(low=0, high=n, shape=(2,), dtype=np.int32)
        # self.action_space = gym.spaces.Discrete(n * n)
        self.action_space = gym.spaces.MultiDiscrete([n, n])

    @staticmethod
    def update_state(state, new_goal):
        state = deepcopy(state)
        state.goal = new_goal
        return state

    @staticmethod
    def calculate_reward(state, action, new_state, info):
        return int(state.goal in info['positions'])

    @staticmethod
    def state_to_goal(state):
        return state.state

    def get_obs(self):
        return np.array(list(self.position) + list(self.goal))

        # return OrderedDict([
        #     ("state", np.array(self.position)),
        #     ("goal", np.array(self.goal))
        # ])

        # return State(self.position, self.goal)

    def seed(self, seed=None):
        if seed is None:
            seed = random.randint(0, 2 ** 256)

        self.rnd = random.Random(seed)
        return [seed]

    def step(self, action: int):
        # print(action)
        # xt = action % self.n
        # yt = action // self.n
        xt, yt = action
        n = self.n

        assert 0 <= xt < n and 0 <= yt < n

        positions = []

        reward = 0
        for i in range(GridParam.multi_step):
            x, y = self.position
            positions.append(self.position)

            if (x, y) == (xt, yt):
                break

            if x < xt:
                x += 1
            elif x > xt:
                x -= 1

            if y < yt:
                y += 1
            elif y > yt:
                y -= 1

            self.position = (x, y)

            if self.position == self.goal:
                reward += 1
                self.goal = get_location((n, n), self.position, self.rnd)

        self.steps += 1

        done = self.steps == GridParam.max_steps

        return self.get_obs(), reward, done, {'positions': positions}

    def reset(self):
        self.position = get_location((self.n, self.n), (-1, -1), self.rnd)
        self.goal = get_location((self.n, self.n), self.position, self.rnd)
        self.steps = 0
        return self.get_obs()

    def render(self, mode='human'):
        screen_width = 400
        screen_height = 400
        separation_width = 2
        square_width = (screen_width - separation_width) / self.n - separation_width
        border = separation_width

        if self.viewer is None:
            from gym.envs.classic_control import rendering

            self.viewer = rendering.Viewer(screen_width, screen_height)

            self.grid = []

            for i in range(self.n):
                row = []
                for j in range(self.n):
                    x0 = border + i * (square_width + separation_width)
                    y0 = border + j * (square_width + separation_width)
                    x1 = x0 + square_width
                    y1 = y0 + square_width
                    square = rendering.FilledPolygon([(x0, y0), (x1, y0), (x1, y1), (x0, y1)])
                    square.set_color(0, 0, 0)
                    self.viewer.add_geom(square)
                    row.append(square)
                self.grid.append(row)

        for i, row in enumerate(self.grid):
            for j, square in enumerate(row):
                if (i, j) == self.position:
                    square.set_color(1., 0., 0.)
                elif (i, j) == self.goal:
                    square.set_color(0., 0., 1.)
                else:
                    square.set_color(0., 0., 0.)

        return self.viewer.render(False)


def get_location(shape, diff, rnd):
    w, h = shape
    while True:
        x, y = rnd.randint(0, w - 1), rnd.randint(0, h - 1)
        if (x, y) != diff:
            break
    return x, y
