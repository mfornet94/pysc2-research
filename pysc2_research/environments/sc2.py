import gym
import numpy as np
from pysc2.env import sc2_env
from pysc2.lib import features, actions

from .base import SC2Environment
from ..sc2common import select_unit, build_state


class MoveToBeaconCoordinates(SC2Environment):
    def __init__(self, visualize):
        env = sc2_env.SC2Env(
            map_name="MoveToBeacon",
            agent_interface_format=features.AgentInterfaceFormat(
                feature_dimensions=features.Dimensions(screen=124,
                                                       minimap=84),
                use_feature_units=True),

            step_mul=8,
            game_steps_per_episode=0,
            visualize=visualize)

        super().__init__(env)

        self.n = 124

        self.observation_space = gym.spaces.MultiDiscrete([self.n, self.n, self.n, self.n])
        self.action_space = gym.spaces.MultiDiscrete([2, self.n, self.n])

    def _step(self, action):
        select, x, y = action

        if select:
            action = select_unit(self.deep_obs)
            action_id = actions.FUNCTIONS.select_point.id
        else:
            action = actions.FUNCTIONS.Move_screen("now", (x, y))
            action_id = actions.FUNCTIONS.Move_screen.id

        if action_id in self.deep_obs.observation.available_actions:
            return action
        else:
            return actions.FUNCTIONS.no_op()

    def build_obs(self):
        # (x_marine, y_marine, x_goal, y_goal)
        state = build_state(self.deep_obs)
        return np.array(list(state.status)[:2] + list(state.goal)).astype(np.int)
