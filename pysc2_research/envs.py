from gym import register

register(
    id='ParametricGridSmall-v0',
    entry_point='pysc2_research.environments.grid_param:GridParam',
    max_episode_steps=120,
    kwargs={"n": 4}
)

register(
    id='ParametricGridLarge-v0',
    entry_point='pysc2_research.environments.grid_param:GridParam',
    max_episode_steps=120,
    kwargs={"n": 8}
)

register(
    id='MoveToBeaconCoordinates-v0',
    entry_point='pysc2_research.environments.sc2:MoveToBeaconCoordinates',
    kwargs={"visualize": False},
)

register(
    id='MoveToBeaconCoordinatesViz-v0',
    entry_point='pysc2_research.environments.sc2:MoveToBeaconCoordinates',
    kwargs={"visualize": True},
)
