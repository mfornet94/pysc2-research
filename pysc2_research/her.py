import numpy as np

from environments.base import Environment


def expand_experience(minibatch, config, env: Environment):
    """
    :param minibatch: List of tuples (s, a, r, sn, info)
        s: state
        a: action
        r: reward
        sn: new state
        info: info to compute reward

    :param config: Parameters to use for HER strategy.
    :param env: Environment used only for static method implementations.
    :return: List of tuples (s, a, r, sn) with new experience
    """

    new_experience = []
    for experience in minibatch:

        if env.is_good_experience(experience) and \
                np.random.random() < config["her"]["density"]:

            s, a, r, sn, info = experience
            new_g = env.state_to_goal(sn)
            new_sn = env.update_state(sn, new_g)
            new_s = env.update_state(s, new_g)
            new_r = env.calculate_reward(new_s, a, new_sn, info)
            updated_exp = (new_s, a, new_r, new_sn, info)

            new_experience.append(updated_exp)
        else:
            new_experience.append(experience)

    return new_experience


if __name__ == '__main__':
    print("""This is no longer the entry point. Use instead:
>>> python main.py""")
