from tensorboardX import SummaryWriter


class History:
    def __init__(self, tbx: SummaryWriter):
        self.tbx = tbx
        self.values = {}
        self.episode = 0

    def add_scalar(self, name, value):
        self.episode += 1
        self.tbx.add_scalar(name, value, self.episode)
        self.get(name).append(value)

    def get(self, name):
        if name not in self.values:
            self.values[name] = []
        return self.values[name]
