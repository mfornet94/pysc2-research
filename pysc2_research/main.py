import sys

from absl import flags

from core import run
from utils import load_config

if __name__ == '__main__':
    # Load config file
    idx = sys.argv.index("--config")
    path = sys.argv[idx + 1]
    config = load_config(path)

    del sys.argv[idx:idx + 2]

    FLAGS = flags.FLAGS
    FLAGS(sys.argv)

    run(config)
