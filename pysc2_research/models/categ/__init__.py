from models.categ.conf import CategoricalModelConfiguration
from models.categ.controller import CategoricalModelController
from models.categ.environment import CategoricalModelEnvironment


def get_package():
    return CategoricalModelConfiguration, CategoricalModelController, CategoricalModelEnvironment
