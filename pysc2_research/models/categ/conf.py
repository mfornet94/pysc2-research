from config import Configuration


class CategoricalModelConfiguration(Configuration):
    update_actor = 10  # Update actor with critic every `update_actor` epochs.
    testing_actor = 1  # Test actor every `testing_actor` epochs.
