import numpy as np
from tensorboardX import SummaryWriter

from buffer import Buffer
from core import evaluate_actor
from loggers import logger_her
from controllers.base import Controller
from models.categ.conf import CategoricalModelConfiguration
from sc2common import build_action
from models.categ.utils import QModel, train, good_action
from models.common import can_do, move_no_op
from environments.base import Environment


class CategoricalModelController(Controller):
    def __init__(self, config: CategoricalModelConfiguration, tbx: SummaryWriter, env: Environment):
        super().__init__(config, tbx, env)

        self.epoch = 1
        self.training_step = 0

        s_g_a = [3, 2, 5]
        self.actor = QModel(s_g_a, config.her)
        self.critic = QModel(s_g_a, config.her)

        if not config.train_from_scratch:
            self.load()
        else:
            logger_her.info("Training QNetworks from scratch")

    def load(self):
        self.actor.load()
        self.critic.load()

    def epoch_step(self):
        if self.epoch % self.config.testing_actor == 0:
            logger_her.info("Evaluate actor")
            mean_reward = evaluate_actor(self)
            self.tbx.add_scalar('eval/mean_reward', mean_reward, self.epoch)

        if self.epoch % self.config.update_actor == 0:
            logger_her.info("Update actor")
            self.actor.update(self.critic)

        self.epoch += 1

    def summary(self):
        self.actor.summary()

    def select_action(self, env, eps):
        action_ix = self.actor.select_action(env.obs, eps)
        real_action = build_action(env.obs, env.deep_obs, action_ix)

        if not can_do(env.deep_obs, real_action):
            real_action = move_no_op()

        return real_action, action_ix

    def train(self, minibatch):
        history = train(self.critic, self.actor, minibatch, self.config)
        final_loss = history.history['loss'][-1]
        self.tbx.add_scalar('train/loss', final_loss, self.training_step)
        self.training_step += 1

        self.critic.log_stats("critic", self.epoch, self.tbx)
        self.actor.log_stats("actor", self.epoch, self.tbx)

    def log_buffer(self, buffer: Buffer, epoch: int):
        # Note: final_count[0] -> number of final states in the buffer
        final_count = np.zeros(2)
        good_actions = 0
        total_reward = 0

        action_counts = [0] * 5
        action_names = "up down left right select".split()

        for s, a, r, sn in buffer.data:
            if good_action(s, a):
                good_actions += 1

            if r != 0:
                final_count[0] += 1
            else:
                final_count[1] += 1

            total_reward += r

            if not isinstance(a, np.int64) and 0 <= a < 5:
                print("a:", a)
                print("type(a):", type(a))
                raise ValueError("Invalid action")

            action_counts[a] += 1

        size = buffer.size

        self.tbx.add_scalar("buffer/good_actions", good_actions, epoch)
        self.tbx.add_scalar("buffer/size", size, epoch)
        self.tbx.add_scalar("buffer/mean_reward", total_reward / size, epoch)
        self.tbx.add_scalar("buffer/final_states_percent", 100 * final_count[0] / size, epoch)

        for a_count, a_name in zip(action_counts, action_names):
            self.tbx.add_scalar("actions/{}".format(a_name), 100 * a_count / size, epoch)
