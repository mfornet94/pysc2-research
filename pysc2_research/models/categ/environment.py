from environments.base import Environment
from models.categ.utils import status_to_goal
from sc2common import build_state, State


class CategoricalModelEnvironment(Environment):
    @staticmethod
    def is_good_experience(experience):
        """
        Check if something change in the state in this experience.

        While using HER avoid inserting experience that gives almost zero information
        to the learning process. Rationale: keep curriculum clean and useful.
        """
        s, a, r, sn = experience
        return (s.status != sn.status).any()

    @staticmethod
    def update(old_experience, new_goal):
        s, a, r, sn = old_experience

        _g = status_to_goal(new_goal[-1].status)
        _sn = State(sn.status.copy(), _g.copy())

        exp = (State(s.status.copy(), _g.copy()),
               a,
               1 if _sn.is_final else 0,
               _sn)

        return exp

    def build_obs(self):
        return build_state(self.deep_obs)
