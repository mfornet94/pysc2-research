from os.path import exists

import numpy as np
from keras.callbacks import ModelCheckpoint
from keras.layers import Input, Dense, Concatenate, Dot
from keras.models import Model
from keras.optimizers import Adam
from keras.utils import to_categorical

from loggers import logger_her
from models.categ.conf import CategoricalModelConfiguration
from utils import model_path


class QModel:
    """ This model learns from a categorical set of actions expected reward of each action on every state
    """

    def __init__(self, s_g_a, her):
        """
            * s_g_a: (state_size, goal_size, action_size).
            * her: bool indicating if her is used or not.
        """
        self.s_g_a = s_g_a
        self.her = her

        self.model_a = None
        self.model_b = None

        self.checkpoint = ModelCheckpoint(model_path(self.s_g_a, self.her))
        self.build()

    def build(self):
        _s, _g, _a = self.s_g_a

        status = Input(shape=(_s,))
        goal = Input(shape=(_g,))
        action = Input(shape=(_a,))

        input_layer = Concatenate()([status, goal])

        hidden = Dense(3, activation='sigmoid', name='first', use_bias=True)(input_layer)
        action_value = Dense(_a, activation='sigmoid', name='second', use_bias=True)(hidden)

        current_value = Dot(-1)([action_value, action])  # Value of the current action

        # Models with shared weights but different I\O
        # model_a is for training
        # model_b if for predicting output

        self.model_a = Model(inputs=[status, goal, action], outputs=[current_value])
        self.model_b = Model(inputs=[status, goal], outputs=[action_value])

        opt = Adam()
        self.model_a.compile(optimizer=opt, loss='mse')

        # This is necessary but useless since this model
        # will never be fitted.
        self.model_b.compile(optimizer=opt, loss='mse')

    def action_value(self, states):
        status = np.array([status for status, goal in states])
        goal = np.array([goal for status, goal in states])
        action_value = self.model_b.predict([status, goal])
        return action_value

    def best_action_value(self, states):
        status = np.array([status for status, goal in states])
        goal = np.array([goal for status, goal in states])
        action_value = self.model_b.predict([status, goal])

        best_value = action_value.max(1)
        best_action = action_value.argmax(1)

        return best_value, best_action

    def select_action(self, state, epsilon_greedy=None):
        if epsilon_greedy is not None and np.random.random() < epsilon_greedy:  # Sample random action
            _, _, _a = self.s_g_a
            action = np.int64(np.random.randint(_a))

        else:  # Sample greedy action
            states = [state]
            status = np.array([status for status, goal in states])
            goal = np.array([goal for status, goal in states])
            action_value = self.model_b.predict([status, goal])

            proba = action_value.flatten() / action_value.sum()
            action = np.int64(np.random.choice(5, p=proba))

        return action

    def train(self, status, goal, action, target, *args, **kwargs):
        _, _, _a = self.s_g_a
        one_hot_action = to_categorical(action, _a)
        return self.model_a.fit([status, goal, one_hot_action], [target], callbacks=[self.checkpoint], *args, **kwargs)

    def update(self, critic):
        self.model_a.set_weights(critic.model_a.get_weights())

    def load(self):
        path = model_path(self.s_g_a, self.her)
        if exists(path):
            try:
                self.model_a.load_weights(path)
                logger_her.info("Model loaded...")
            except OSError:
                pass

    def summary(self):
        self.model_a.summary()
        self.model_b.summary()

    def log_stats(self, model_name, epoch, tbx):
        weights = []

        for w in self.model_a.get_weights():
            weights += list(w.flatten())

        std = np.std(weights)
        mean = np.mean(weights)

        weights.sort()
        per = [(str(i), np.percentile(weights, i)) for i in range(0, 101, 20)]

        tbx.add_scalar("weights/{}-std".format(model_name), std, epoch)
        tbx.add_scalar("weights/{}-mean".format(model_name), mean, epoch)

        for name, value in per:
            tbx.add_scalar("percentile/{}-{}".format(model_name, name), value, epoch)


def train(critic, actor, minibatch, args: CategoricalModelConfiguration):
    status = np.array([state.status for (state, action, reward, n_state) in minibatch])
    goal = np.array([state.goal for (state, action, reward, n_state) in minibatch])
    action = np.array([action for (state, action, reward, n_state) in minibatch])
    target = np.zeros(len(minibatch))

    states = [state for (state, action, reward, n_state) in minibatch]
    n_states = [n_state for (state, action, reward, n_state) in minibatch]

    value = critic.action_value(states)
    n_value, _ = actor.best_action_value(n_states)

    for idx, (_, action_, reward_, _) in enumerate(minibatch):
        Q_s_a = value[idx][action_]
        n_Q_s_a = reward_ + args.discount * n_value[idx]
        target[idx] = Q_s_a + args.alpha * (n_Q_s_a - Q_s_a)

    return critic.train(status, goal, action, target, verbose=args.train_verbose)


def status_to_goal(status):
    sX, sY, S = status
    goal = [sX, sY]
    return goal


def good_action(state, action):
    """
    actions
    0 --> up
    1 --> down
    2 --> left
    3 --> right
    4 --> select
    """
    up, down, left, rigth, select = 0, 1, 2, 3, 4
    x1, x2, s = state.status
    g1, g2 = state.goal

    if s == 0:
        return action == select
    if x1 <= g1:
        if x2 > g2:
            return action in {up, rigth}
        else:
            return action in {down, rigth}
    else:
        if x2 >= g2:
            return action in {up, left}
        else:
            return action in {down, left}
