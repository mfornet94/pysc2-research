from pysc2.lib import actions


def can_do(obs, action):
    return action[0] in obs.observation.available_actions


def move_no_op():
    return actions.FUNCTIONS.no_op()
