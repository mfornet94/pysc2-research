from models.parametric.conf import ParametricConfiguration
from models.parametric.controller import ParametricModelController
from models.parametric.environment import ParametricEnvironment


def get_package():
    return ParametricConfiguration, ParametricModelController, ParametricEnvironment
