from keras import Input, Model
from keras.layers import Reshape, Dense, concatenate

from models.parametric import ParametricConfiguration


def build_embedding_layer(embedding_size):
    i = Input((embedding_size,))
    o = Reshape((embedding_size,))(i)
    model = Model(inputs=[i], outputs=[o], name="Embedding")
    return model


def build_action_layer(embedding_size, action_size, parameters_size):
    embed = Input((embedding_size,))
    action = Dense(action_size, activation='softmax', name='action')(embed)
    parameters = Dense(parameters_size, name='parameters')(embed)
    model = Model(inputs=[embed], outputs=[action, parameters], name="Actor")
    return model


def build_critic_layer(embedding_size, action_size, parameters_size, config: ParametricConfiguration):
    # Input
    embed = Input((embedding_size,))
    action = Input((action_size,))
    parameters = Input((parameters_size,))

    full_house = concatenate([embed, action, parameters])

    if config.version == "0.1":
        value = Dense(1)(full_house)
    elif config.version == "0.2":
        hidden = Dense(16, activation='sigmoid')(full_house)
        hidden = Dense(16, activation='sigmoid')(hidden)
        hidden = Dense(16, activation='relu')(hidden)
        value = Dense(1)(hidden)

    # Output
    model = Model(inputs=[embed, action, parameters], outputs=[value], name="Critic")
    return model
