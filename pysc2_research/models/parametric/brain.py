import numpy as np
from keras import Model

from models.parametric import ParametricConfiguration


def train_critic(model: Model, minibatch, args: ParametricConfiguration):
    s = np.array([s for (s, a, r, sn) in minibatch])
    r = np.array([[r] for (s, a, r, sn) in minibatch])
    sn = np.array([sn for (s, a, r, sn) in minibatch])
    vsn = model.predict(sn)
    target = r + args.discount * vsn

    value = model.predict(s)

    top = 10
    info = np.concatenate([s, r, sn, value, target, vsn], -1)[:top]
    print(info)

    # print("state:", s[:top])
    # print("reward:", r[:top])
    # print("state_n:", sn[:top])
    # print("value_state_n:", vsn[:top])
    # print("target:", target[:top])

    return model.fit(s, target, verbose=args.train_verbose)
