"""
    Architecture Versions details

    0.1:
        Critic: Linear function
        Actor: Optimal (hardcoded) policy
        Embed: Identity
"""
from config import Configuration


class ParametricConfiguration(Configuration):
    # Use optimal policy for MoveToBeacon for now
    use_hard_coded_policy = True

    # Optimizer used for `train_critic`
    optimizer = 'rmsprop'

    # Architecture
    version = "0.1"

    # Training
    save_every = 1
