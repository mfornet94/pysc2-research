import numpy as np
from keras import Input, Model
from pysc2.lib import actions
from tensorboardX import SummaryWriter

from controllers.base import Controller
from models.common import can_do, move_no_op
from models.parametric.architecture import build_embedding_layer, build_action_layer, build_critic_layer
from models.parametric.brain import train_critic
from models.parametric.conf import ParametricConfiguration
from models.parametric.utils import select_unit


class ParametricModelController(Controller):
    def __init__(self, config: ParametricConfiguration, tbx: SummaryWriter, env):
        super().__init__(config, tbx, env)

        self.actor = None
        self.critic = None

        self.build()

        self.epoch = 1

        if config.use_hard_coded_policy:
            self.hard_coded_policy()

    def build(self):
        E = build_embedding_layer(5)  # Embedding
        A = build_action_layer(5, 2, 2)  # Actor
        C = build_critic_layer(5, 2, 2, self.config)  # Critic

        inp = Input((5,))
        embed = E(inp)
        (action, parameter) = A(embed)
        value = C([embed, action, parameter])

        self.actor = Model(inputs=[inp], outputs=[action, parameter])
        self.critic = Model(inputs=[inp], outputs=[value])

    def summary(self):
        self.actor.summary()
        self.critic.summary()

    def select_action(self, env, eps):
        if eps is not None and np.random.random() < eps:
            # Get random action
            action = np.random.random((2,))
            enc_action = np.random.random((2,))
        else:
            # Get action from model
            state = env.obs.reshape(1, -1)
            action, parameter = self.actor.predict(state)

            action = action.flatten()
            enc_action = parameter.flatten()

        if action[0] > action[1]:
            # Select unit
            real_action = select_unit(env.deep_obs)
        else:
            # Move unit
            x, y = enc_action
            real_action = actions.FUNCTIONS.Move_screen("now", (x, y))

        if not can_do(env.deep_obs, real_action):
            real_action = move_no_op()

        return real_action, enc_action

    def train(self, minibatch):
        pass

    def train_critic(self, minibatch):
        if not hasattr(self, "started_train_critic"):
            self.critic.get_layer('Embedding').trainable = False
            self.critic.get_layer('Actor').trainable = False
            self.critic.get_layer('Critic').trainable = True
            self.critic.compile(optimizer=self.config.optimizer, loss='mse')
            self.started_train_critic = True
            self.critic.summary()

        history = train_critic(self.critic, minibatch, self.config)
        loss = history.history['loss'][-1]
        self.tbx.add_scalar("loss", loss, self.epoch)

    def epoch_step(self):
        config: ParametricConfiguration = self.config
        if self.epoch % config.save_every == 0:
            critic: Model = self.critic
            critic.save_weights("{}/param-e{}-v{}.nn".format(config.saved_models_path, self.epoch, config.version),
                                overwrite=True)

        self.epoch += 1

    def hard_coded_policy(self):
        self.actor.get_layer('Actor').get_layer('action').set_weights(
            [
                np.array([[0, 0],
                          [0, 0],
                          [-20, 20],
                          [0, 0],
                          [0, 0]]),
                np.array([10, -10])
            ]
        )
        self.actor.get_layer('Actor').get_layer('parameters').set_weights(
            [
                np.array([[0, 0],
                          [0, 0],
                          [0, 0],
                          [1, 0],
                          [0, 1]]),
                np.array([0, 0])
            ]
        )
