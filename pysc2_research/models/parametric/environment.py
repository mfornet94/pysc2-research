import numpy as np
from pysc2.lib import units
from copy import deepcopy

from models.parametric.utils import unit_type_is_selected, locate_unit, locate_goal
from environments.base import Environment


class ParametricEnvironment(Environment):
    def build_obs(self):
        obs = np.zeros(5)

        # marine position
        obs[0], obs[1] = locate_unit(self.deep_obs)
        # is unit selected
        obs[2] = 1 if unit_type_is_selected(self.deep_obs, units.Terran.Marine) else 0
        # beacon position
        obs[3], obs[4] = locate_goal(self.deep_obs)

        return obs

    @staticmethod
    def is_good_experience(experience):
        return True

    @staticmethod
    def update(old_experience, new_goal):
        new_experience = deepcopy(old_experience)
        sn = new_goal[-1]
        goal = sn[3:]
        new_experience[0][3:] = goal
        new_experience[-1][3:] = goal
        return new_experience
