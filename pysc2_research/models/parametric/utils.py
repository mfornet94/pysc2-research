import random

import numpy as np
from pysc2.lib import units, actions, features


def unit_type_is_selected(obs, unit_type):
    if len(obs.observation.single_select) > 0 and obs.observation.single_select[0].unit_type == unit_type:
        return True
    if len(obs.observation.multi_select) > 0 and obs.observation.multi_select[0].unit_type == unit_type:
        return True
    return False


def locate_unit(obs):
    marine = [unit for unit in obs.observation.feature_units if unit.unit_type == units.Terran.Marine]
    if len(marine) > 0:
        marine = random.choice(marine)

        return marine.x, marine.y
    return -1, -1


def locate_goal(obs):
    player_relative = obs.observation.feature_screen.player_relative
    beacon = _xy_locs(player_relative == PLAYER_NEUTRAL)
    if not beacon:
        return -1, -1
    beacon_center = np.mean(beacon, axis=0).round()
    return beacon_center


def _xy_locs(mask):
    """Mask should be a set of bools from comparison with a feature layer."""
    y, x = mask.nonzero()
    return list(zip(x, y))


def select_unit(obs):
    marine = [unit for unit in obs.observation.feature_units if unit.unit_type == units.Terran.Marine]
    if len(marine) > 0:
        marine = random.choice(marine)

        return actions.FUNCTIONS.select_point("select_all_type", (marine.x, marine.y))
    return actions.FUNCTIONS.no_op()


PLAYER_NEUTRAL = features.PlayerRelative.NEUTRAL  # beacon/minerals