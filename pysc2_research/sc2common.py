import collections
import random

import numpy as np
from pysc2.lib import features, actions, units

from .models.common import move_no_op

_PLAYER_NEUTRAL = features.PlayerRelative.NEUTRAL  # beacon/minerals
ACTIONS = {
    'MoveUp': 0,
    'MoveDown': 1,
    'MoveLeft': 2,
    'MoveRight': 3,
    'Select_Unit': 4,
    'Nop': -1,
}
SHIFT = 5


def move_random():
    x = random.randint(0, 84)
    y = random.randint(0, 84)
    return actions.FUNCTIONS.Move_screen("now", (x, y))


def move_left(pos):
    x, y = pos
    x = x - SHIFT if x - SHIFT >= 0 else 0
    return actions.FUNCTIONS.Move_screen("now", (x, y))


def move_up(pos):
    x, y = pos
    y = y - SHIFT if y - SHIFT >= 0 else 0
    return actions.FUNCTIONS.Move_screen("now", (x, y))


def move_down(pos):
    x, y = pos
    y = y + SHIFT
    return actions.FUNCTIONS.Move_screen("now", (x, y))


def move_right(pos):
    x, y = pos
    x = x + SHIFT
    return actions.FUNCTIONS.Move_screen("now", (x, y))


def select_unit(obs):
    marine = [unit for unit in obs.observation.feature_units if unit.unit_type == units.Terran.Marine]
    if len(marine) > 0:
        marine = random.choice(marine)

        return actions.FUNCTIONS.select_point("select_all_type", (marine.x, marine.y))
    return actions.FUNCTIONS.no_op()


def build_state(obs):
    state = np.zeros(3)
    goal = np.zeros(2)
    state[2] = 1 if unit_type_is_selected(obs, units.Terran.Marine) else 0
    state[0], state[1] = locate_unit(obs)
    goal[0], goal[1] = locate_goal(obs)
    return State(state, goal)


def unit_type_is_selected(obs, unit_type):
    if len(obs.observation.single_select) > 0 and obs.observation.single_select[0].unit_type == unit_type:
        return True
    if len(obs.observation.multi_select) > 0 and obs.observation.multi_select[0].unit_type == unit_type:
        return True
    return False


def locate_unit(obs):
    marine = [unit for unit in obs.observation.feature_units if unit.unit_type == units.Terran.Marine]
    if len(marine) > 0:
        marine = random.choice(marine)

        return marine.x, marine.y
    return -1, -1


def locate_goal(obs):
    player_relative = obs.observation.feature_screen.player_relative
    beacon = _xy_locs(player_relative == _PLAYER_NEUTRAL)
    if not beacon:
        return -1, -1
    beacon_center = np.mean(beacon, axis=0).round()
    return beacon_center


def _xy_locs(mask):
    """Mask should be a set of bools from comparison with a feature layer."""
    y, x = mask.nonzero()
    return list(zip(x, y))


def build_action(state, obs, action):
    pos = state.status[0], state.status[1]

    if ACTIONS["Nop"] == action:
        return move_no_op()

    if ACTIONS["MoveUp"] == action:
        return move_up(pos)

    if ACTIONS["MoveDown"] == action:
        return move_down(pos)

    if ACTIONS["MoveLeft"] == action:
        return move_left(pos)

    if ACTIONS["MoveRight"] == action:
        return move_right(pos)

    if ACTIONS["Select_Unit"] == action:
        return select_unit(obs)


class State(collections.namedtuple("_State", ("status", "goal"))):
    @property
    def is_final(self):
        X, Y, _ = self.status
        Xg, Yg = self.goal
        return abs(X - Xg) <= 5 and abs(Y - Yg) <= 5

    def __str__(self):
        return '\n'.join(str(x) for x in self)

    def as_np(self):
        return np.array(list(self.status) + list(self.goal))
