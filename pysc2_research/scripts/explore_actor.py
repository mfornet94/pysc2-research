import numpy as np
from keras import Model

from models.parametric import ParametricModelController, ParametricConfiguration


def load_model(model, epoch):
    model.load_weights('../saved_models/parametric-00/param-e{}-v0.1.nn'.format(epoch))


def get_weights(model):
    h, b = model.get_layer('Critic').get_weights()
    b = np.array([b])
    return list(np.round(np.concatenate([h.flatten(), b.flatten()]), 2))


def main():
    config = ParametricConfiguration()
    controller = ParametricModelController(config, None, None)
    model: Model = controller.critic

    for i in range(1, 190, 10):
        load_model(model, i)
        print(get_weights(model))


if __name__ == '__main__':
    main()
