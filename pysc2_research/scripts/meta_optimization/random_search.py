import json
import os
from copy import deepcopy
from datetime import datetime
from os.path import join
from pprint import pprint

import numpy as np

from core import run
from utils import load_config

options = {
    'discount': [.9, .99, .999],
    'actor_lr': [.1, .01, .001],
    'critic_lr_fak': [1., 5., 10.],
    'regularization': [.1, .01, .001, 0.],
    'entropy_init': [1., .1, .01, .001],
    'entropy_end_fak': [1., 10., 100., 1000.],
    'entropy_time': [1, 100, 1000, 10000],
    'continuous_update': [True, False],
    'alpha_actor': [.001, .0001, .00001],
    'alpha_critic_fak': [1., 10., 100.],
    'update_actor_every': [100, 300, 1000, 3000],
    'update_critic_every_fak': [100, 300, 1000, 3000],
    'batch_size': [1, 4, 16, 32],
    'her_density': [0., .1, .5, .9],
}

updates = [
    lambda config: config['controller']['kwargs'].update(
        {'critic_lr': config['controller']['kwargs']['actor_lr'] * config['controller']['kwargs']['critic_lr_fak']}),

    lambda config: config['controller']['kwargs'].update(
        {'entropy_end': config['controller']['kwargs']['entropy_init'] / config['controller']['kwargs'][
            'entropy_end_fak']}),

    lambda config: config['controller']['kwargs'].update(
        {'alpha_critic': config['controller']['kwargs']['alpha_actor'] / config['controller']['kwargs'][
            'alpha_critic_fak']}),

    lambda config: config['rl']['train'].update(
        {'batch_size': config['controller']['kwargs']['batch_size']}),

    lambda config: config['her'].update(
        {'density': config['controller']['kwargs']['her_density']}),
]


def run_training(config):
    print("Start training")
    pprint(config)
    config['name'] = '{}'.format(str(datetime.now()))

    path = 'configs/{}'.format(config['__time'])
    os.makedirs(path, exist_ok=True)
    file_path = '{}/{}.json'.format(path, config['name'])

    with open(file_path, 'w') as f:
        json.dump(config, f)

    return run(config)


def random_search(config):
    while True:
        _config = deepcopy(config)

        for key, values in options.items():
            idx = np.random.choice(len(values))
            val = values[idx]
            _config['controller']['kwargs'][key] = val

        for upd in updates:
            upd(_config)

        run_training(_config)


# Explore each parameter independently with a good set of parameters
# def linear_search():
#     pass


def main():
    config = load_config("../../configs/toy.json")

    EPISODES = 1000

    # config['her']['density'] = 0.
    config['environment']['kwargs']['n'] = 8
    config['controller']['kwargs']['n'] = config['environment']['kwargs']['n']
    config['__time'] = str(datetime.now())
    config['tensorboard']['path'] = join('tb_logs', config['__time'])
    config['rl']['train']['episodes'] = EPISODES
    config['verbose'] = False

    random_search(config)


if __name__ == '__main__':
    main()
