# Create a model that works as a very good policy to
# the MoveToBeacon minigame.
# 
# Note: This code was meant to run on the `her` folder
# next to `main.py`

import numpy as np
from keras import Model, Sequential
from keras.layers import Input, Dense, Concatenate, Dot

from utils import model_path


def get_model():
    model = Sequential()
    model.add(Dense(3, activation='sigmoid', input_shape=(5,), use_bias=True, name='first'))
    model.add(Dense(5, activation='sigmoid', use_bias=True, name='second'))
    model.get_layer('first').set_weights(
        [
            np.array([
                [0, 10, 0],
                [0, 0, 10],
                [10, 0, 0],
                [0, -10, 0],
                [0, 0, -10],
            ]),
            np.array([-5, 0, 0])
        ]
    )

    model.get_layer('second').set_weights(
        [
            np.array([[10, 15, 10, 15, -10],
                      [0, 0, 10, -10, 0],
                      [10, -10, 0, 0, 0]
                      ]),
            np.array([-15, -10, -15, -10, 5])
        ]
    )


def build():
    status = Input(shape=(3,))
    goal = Input(shape=(2,))
    action = Input(shape=(5,))

    input_layer = Concatenate()([status, goal])

    hidden = Dense(3, activation='sigmoid', name='first')(input_layer)
    action_value = Dense(5, activation='sigmoid', name='second')(hidden)

    current_value = Dot(-1)([action_value, action])

    model_a = Model(inputs=[status, goal, action], outputs=[current_value])

    model_a.get_layer('first').set_weights(
        [
            np.array([
                [0, 10, 0],
                [0, 0, 10],
                [10, 0, 0],
                [0, -10, 0],
                [0, 0, -10],
            ]),
            np.array([-5, 0, 0])
        ]
    )

    model_a.get_layer('second').set_weights(
        [
            np.array([[10, 15, 10, 15, -10],
                      [0, 0, 10, -10, 0],
                      [10, -10, 0, 0, 0]
                      ]),
            np.array([-15, -10, -15, -10, 5])
        ]
    )

    model_a.save_weights(model_path([3, 2, 5], True), overwrite=True)


if __name__ == '__main__':
    build()
