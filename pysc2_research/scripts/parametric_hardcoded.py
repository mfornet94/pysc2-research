""" Build and test hardcoded policy for Parametric model
"""

import numpy as np

from models.parametric import ParametricModelController, ParametricConfiguration


def main():
    config = ParametricConfiguration()
    controller = ParametricModelController(config, None, None)
    controller.summary()

    model = controller.critic

    actor = model.get_layer('Actor')
    action = actor.get_layer('action')
    action.set_weights(
        [
            np.array([[0, 0],
                      [0, 0],
                      [-20, 20],
                      [0, 0],
                      [0, 0]]),
            np.array([10, -10])
        ]
    )
    parameters = actor.get_layer('parameters')
    parameters.set_weights(
        [
            np.array([[0, 0],
                      [0, 0],
                      [0, 0],
                      [1, 0],
                      [0, 1]]),
            np.array([0, 0])
        ]
    )

    for name in "Embedding Actor Critic".split():
        layer = model.get_layer(name)
        print("\n{}".format(name))
        print(layer.get_weights())

    print(actor.predict(np.array([5, 5, 1, 10, 10]).reshape(1, -1)))


if __name__ == '__main__':
    main()
