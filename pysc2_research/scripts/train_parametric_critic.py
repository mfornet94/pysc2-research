"""
    Train critic in parametric space using hardcoded policy.

    Algorithm:

    * actor = initialize optimal policy
    * critic = initialize random critic

    for i in range(oo):
        * episode = sample_episode using epsilon greedy actor (for exploration)
        * store episode in buffer
        * minibatch = sample minibatch from buffer
        * train critic on minibatch
"""
from datetime import datetime
from os import mkdir
from os.path import join, exists

from pysc2.env import sc2_env
from pysc2.lib import features
from tensorboardX import SummaryWriter

from buffer import Buffer
from core import sample_episode
from models.parametric import ParametricModelController, ParametricConfiguration, ParametricEnvironment


def main():
    config = ParametricConfiguration()
    config.saved_models_path = join('..', config.saved_models_path, 'parametric-02')
    config.discount = 0.99

    if not exists(config.saved_models_path):
        mkdir(config.saved_models_path)

    tbx = SummaryWriter(log_dir=join(config.tb_logs_path, '{}-{}'.format(config.name, datetime.now())))

    with sc2_env.SC2Env(
            map_name=config.map_name,
            agent_interface_format=features.AgentInterfaceFormat(
                feature_dimensions=features.Dimensions(screen=124, minimap=84),
                use_feature_units=True),
            step_mul=config.step_mul,
            game_steps_per_episode=0,
            visualize=config.visualize) as env:
        wenv = ParametricEnvironment(env)

        controller = ParametricModelController(config, tbx, wenv)
        controller.summary()

        re_buffer = Buffer(config.buffer_size)

        for epoch in range(1, config.epochs + 1):
            _, episode = sample_episode(controller, epsilon_greedy=True)
            re_buffer.add(episode)
            minibatch = re_buffer.sample(config.batch_size)
            controller.train_critic(minibatch)
            controller.epoch_step()


if __name__ == '__main__':
    import sys
    from absl import flags

    FLAGS = flags.FLAGS
    FLAGS(sys.argv)

    main()
