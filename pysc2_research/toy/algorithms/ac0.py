""" Actor Critic.
    Simple version

    Roadmap:
        - Target network
        - Experience replay
"""
import numpy as np
from gym import Env
from keras import Sequential
from keras.layers import Dense
from keras.optimizers import Adam

from toy.grid import Grid

DISCOUNT = .99
ACTOR_LR = 0.001
CRITIC_LR = 0.01


class Actor:
    def __init__(self, lr):
        self.lr = lr
        self.model = Sequential()
        self.model.add(Dense(20,
                             activation='relu',
                             input_shape=(4,)))

        self.model.add(Dense(4,
                             activation='softmax'))

        self.opt = Adam(lr=lr)
        self.model.compile(optimizer=self.opt, loss='categorical_crossentropy')

    def choose_action(self, s):
        proba = self.model.predict(s.reshape(1, -1))
        proba = proba.flatten()
        action = np.random.choice(4, p=proba)
        return action

    def learn(self, s, a, td_error):
        cat_a = np.zeros((1, 4))
        cat_a[0, a] = td_error
        self.model.train_on_batch(s.reshape(1, -1), cat_a)

    def save(self, path):
        self.model.save_weights(path)


class Critic:
    def __init__(self, lr):
        self.model = Sequential()
        self.model.add(Dense(20,
                             activation='relu',
                             input_shape=(4,)))
        self.model.add(Dense(1))
        self.opt = Adam(lr=lr)
        self.model.compile(optimizer=self.opt, loss='mse')

    def learn(self, s, r, s_):
        ss = np.concatenate([s, s_]).reshape(2, -1)
        values = self.model.predict(ss)
        vs = values[:1]
        vs_ = values[1:]
        t = r + DISCOUNT * vs_
        td_error = t - vs

        self.model.train_on_batch(s.reshape(1, -1), t)
        return td_error

    def save(self, path):
        self.model.save_weights(path)


def learn(env: Env):
    actor = Actor(lr=ACTOR_LR)
    critic = Critic(lr=CRITIC_LR)

    RENDER = False
    SAVE = False

    episode = 0
    running_reward = None

    # Keep training forever
    while True:
        s = env.reset()
        s = np.array(s)
        track_r = []
        done = False

        while not done:
            if RENDER: env.render()

            a = actor.choose_action(s)
            s_, r, done, _ = env.step(a)
            s_ = np.array(s_)
            track_r.append(r)

            td_error = critic.learn(s, r, s_)
            actor.learn(s, a, td_error)

            s = s_

        ep_rs_sum = sum(track_r)

        if running_reward is None:
            running_reward = ep_rs_sum
        else:
            running_reward = running_reward * 0.95 + ep_rs_sum * 0.05

        if running_reward > 20:
            RENDER = True

        episode += 1
        print("episode:", episode, "  reward:", int(running_reward))

        if SAVE and episode % 100 == 0:
            actor.save("models/ac0-actor-{}.nn".format(episode))
            critic.save("models/ac0-critic-{}.nn".format(episode))


if __name__ == '__main__':
    grid = Grid(8, 8)
    learn(grid)
