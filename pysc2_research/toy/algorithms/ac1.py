""" Actor Critic.
    Simple version

    Road Map:
        - HER
        - Target network
        - Experience replay (train with shuffled batches)
"""

import numpy as np
import tensorflow as tf

from environments.grid_param import GridParam

DISCOUNT = .99
ACTOR_LR = 0.001
CRITIC_LR = 0.05
EPSILON = 0.1

SIZE = 4


# TODO: Move session inside brain
# TODO: Try with stable-baselines
# TODO: Try with baselines
class Brain:
    def __init__(self, n, critic_lr=CRITIC_LR, actor_lr=ACTOR_LR, discount=DISCOUNT, epsilon=EPSILON):
        self.n = n
        self.critic_lr = critic_lr
        self.actor_lr = actor_lr
        self.discount = discount
        self.epsilon = epsilon

        self._build()

    def _build(self):
        self.state = tf.placeholder(tf.float32, shape=(None, 4))
        self.action = tf.placeholder(tf.float32, shape=(None, 2))

        self.action_value = self.critic(self.state, self.action)
        self.action_pred = self.actor(self.state)

        # Loss Q State Action Value
        self.target_value = tf.placeholder(tf.float32, shape=(None, 1))
        diff = tf.squared_difference(self.target_value, self.action_value)
        loss_q_s_a = tf.reduce_mean(diff)

        critic_optimizer = tf.train.AdamOptimizer(learning_rate=self.critic_lr)
        critic_parameters = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "critic")
        self.critic_opt = critic_optimizer.minimize(loss_q_s_a, var_list=critic_parameters)

        # State value
        self.value = self.critic(self.state, self.actor(self.state))

        actor_optimizer = tf.train.AdamOptimizer(learning_rate=self.actor_lr)
        actor_parameters = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "actor")
        self.actor_opt = actor_optimizer.minimize(-self.value, var_list=actor_parameters)

    @staticmethod
    def critic(state, action):
        with tf.variable_scope('critic', reuse=tf.AUTO_REUSE):
            full = tf.concat([state, action], 1)
            hidden = tf.layers.dense(full, 20, activation=tf.nn.relu, name='hidden')
            action_value = tf.layers.dense(hidden, 1, name='value')
        return action_value

    def actor(self, state):
        with tf.variable_scope('actor', reuse=tf.AUTO_REUSE):
            hidden = tf.layers.dense(state, 20, activation=tf.nn.relu)
            action_pred = tf.layers.dense(hidden, 2)
            action_pred = tf.clip_by_value(action_pred, 0., self.n - 1)
        return action_pred

    def choose_action(self, state, sess: tf.Session):
        # Hardcoded optimal policy
        _state = state.flatten()
        return _state[-2], _state[-1]

        if np.random.random() < self.epsilon:
            x, y = np.random.randint(0, self.n, 2)
        else:
            action = sess.run(self.action_pred, feed_dict={self.state: state})
            action = action.flatten()
            x, y = map(int, action)
        return x, y

    def train_actor(self, state, sess: tf.Session):
        sess.run(self.actor_opt, feed_dict={self.state: state})

    def train_critic(self, state, action, reward, next_state, sess: tf.Session):
        next_state_value = sess.run(self.value, feed_dict={self.state: next_state})
        target = reward + self.discount * next_state_value
        sess.run(self.critic_opt, feed_dict={self.state: state, self.action: action, self.target_value: target})


def tonp(data):
    return np.array(data).reshape(1, -1)


def learn(env):
    brain = Brain(SIZE)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        episode = 0
        smooth_reward = None

        print("Episode | Reward | Smooth reward | Different actions | Most frequent actions | Steps")

        while True:
            done = False
            state = env.reset()
            state = tonp(state)

            total_reward = 0
            actions = {}
            steps = 0

            while not done:
                action = brain.choose_action(state, sess)
                next_state, reward, done, _ = env.step(action)
                total_reward += reward

                actions[action] = actions.get(action, 0) + 1

                next_state, reward, action = map(tonp, [next_state, reward, action])

                brain.train_critic(state, action, reward, next_state, sess)
                brain.train_actor(state, sess)

                state = next_state
                steps += 1

            episode += 1

            if smooth_reward is None:
                smooth_reward = total_reward
            else:
                smooth_reward = .95 * smooth_reward + .05 * total_reward

            different_actions = len(actions)
            most_frequent = max(v for (k, v) in actions.items())

            print("E: {} R: {} Sr: {} Da: {} Mfa: {} S: {}".format(
                episode, total_reward, smooth_reward, different_actions, most_frequent, steps
            ))


if __name__ == '__main__':
    env = GridParam(SIZE)
    env.multi_step = 1
    learn(env)
