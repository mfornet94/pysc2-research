""" Train Critic and compare with QTables using epsilon-optimal actor
"""

import numpy as np
import tensorflow as tf

from environments.grid_param import GridParam

DISCOUNT = .99
ACTOR_LR = 0.001
CRITIC_LR = 0.1
EPSILON = 0.

SIZE = 4


def tonp(data):
    return np.array(data).reshape(1, -1)


class QTable:
    def __init__(self):
        self._table = {}

    @staticmethod
    def fix(data: np.ndarray):
        data = data.flatten()

        if data.shape[0] == 1:
            return int(data)
        else:
            return tuple(data)

    def update(self, s, a, r, s_, a_):
        s, a, r, s_, a_ = map(QTable.fix, [s, a, r, s_, a_])

        vs = self._table.get((s, a), 0.)
        vs_ = self._table.get((s_, a_), 0.)
        diff = r + DISCOUNT * vs_ - vs
        new_vs = vs + CRITIC_LR * diff
        self._table[(s, a)] = new_vs

        return diff

    @property
    def size(self):
        return len(self._table)


class Brain:
    def __init__(self, n, critic_lr=CRITIC_LR, actor_lr=ACTOR_LR, discount=DISCOUNT, epsilon=EPSILON):
        self.n = n
        self.critic_lr = critic_lr
        self.actor_lr = actor_lr
        self.discount = discount
        self.epsilon = epsilon

        self._build()

    def _build(self):
        self.state = tf.placeholder(tf.float32, shape=(None, 4))
        self.action = tf.placeholder(tf.float32, shape=(None, 2))

        self.action_value = self.critic(self.state, self.action)
        self.action_pred = self.actor(self.state)

        # Loss Q State Action Value
        self.target_value = tf.placeholder(tf.float32, shape=(None, 1))
        diff = tf.squared_difference(self.target_value, self.action_value)
        self.loss_q_s_a = tf.reduce_mean(diff)

        critic_optimizer = tf.train.AdamOptimizer(learning_rate=self.critic_lr)
        critic_parameters = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "critic")
        self.critic_opt = critic_optimizer.minimize(self.loss_q_s_a, var_list=critic_parameters)

        # State value
        self.value = self.critic(self.state, self.actor(self.state))

        actor_optimizer = tf.train.AdadeltaOptimizer(learning_rate=self.actor_lr)
        actor_parameters = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "actor")
        self.actor_opt = actor_optimizer.minimize(-self.value, var_list=actor_parameters)

        self.q_table = QTable()

    @staticmethod
    def critic(state, action):
        with tf.variable_scope('critic', reuse=tf.AUTO_REUSE):
            full = tf.concat([state, action], 1)
            hidden0 = tf.layers.dense(full, 40, activation=tf.nn.relu, name='hidden0')
            hidden1 = tf.layers.dense(hidden0, 20, activation=tf.nn.relu, name='hidden1')
            action_value = tf.layers.dense(hidden1, 1, name='value')
        return action_value

    def actor(self, state):
        with tf.variable_scope('actor', reuse=tf.AUTO_REUSE):
            hidden = tf.layers.dense(state, 30, activation=tf.nn.sigmoid)
            action_pred = tf.layers.dense(hidden, 2)
            action_pred = tf.clip_by_value(action_pred, 0., self.n - 1)
        return action_pred

    def choose_action(self, state, sess: tf.Session):
        if np.random.random() < self.epsilon:
            x, y = np.random.randint(0, self.n, 2)
        else:
            # Hardcoded optimal policy
            _state = state.flatten()
            x, y = _state[-2], _state[-1]
        return x, y

    def train_actor(self, state, sess: tf.Session):
        sess.run(self.actor_opt, feed_dict={self.state: state})

    def train_critic(self, state, action, reward, next_state, sess: tf.Session):
        next_action = self.choose_action(next_state, sess)
        next_action = tonp(next_action)
        qtable_loss = self.q_table.update(state, action, reward, next_state, next_action)

        next_state_value = sess.run(self.action_value, feed_dict={self.state: next_state, self.action: next_action})
        target = reward + self.discount * next_state_value
        deep_loss, _ = sess.run([self.loss_q_s_a, self.critic_opt],
                                feed_dict={self.state: state, self.action: action, self.target_value: target})

        return qtable_loss, deep_loss


class SmoothValue:
    def __init__(self):
        self._value = None

    def update(self, x):
        if self._value is None:
            self._value = x
        else:
            self._value = .99 * self._value + .01 * x

    @property
    def value(self):
        return self._value


def learn(env):
    brain = Brain(SIZE)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        episode = 0
        smooth_reward = SmoothValue()
        smooth_qtable_loss_acc = SmoothValue()
        smooth_deep_loss_acc = SmoothValue()
        updates_total = 0

        print("Episode | Reward | Smooth reward | Different actions | Most frequent actions | Steps")

        while True:
            done = False
            state = env.reset()
            state = tonp(state)

            total_reward = 0
            # actions = {}
            # steps = 0

            qtable_loss_acc = 0.
            deep_loss_acc = 0.

            while not done:
                action = brain.choose_action(state, sess)
                next_state, reward, done, _ = env.step(action)
                total_reward += reward

                # actions[action] = actions.get(action, 0) + 1

                next_state, reward, action = map(tonp, [next_state, reward, action])

                qtable_loss, deep_loss = brain.train_critic(state, action, reward, next_state, sess)

                updates_total += 1
                qtable_loss_acc += qtable_loss
                deep_loss_acc += deep_loss
                # brain.train_actor(state, sess)

                state = next_state
                # steps += 1

            episode += 1

            smooth_reward.update(total_reward)
            smooth_qtable_loss_acc.update(qtable_loss_acc)
            smooth_deep_loss_acc.update(deep_loss_acc)

            # different_actions = len(actions)
            # most_frequent = max(v for (k, v) in actions.items())

            print("qtable: {:<8} deepqn: {:<8} updates: {:<8} states: {}"
                  .format(round(smooth_qtable_loss_acc.value, 3),
                          round(smooth_deep_loss_acc.value, 3),
                          updates_total, brain.q_table.size)
                  )

            # print("E: {} R: {} Sr: {} Da: {} Mfa: {} S: {}".format(
            #     episode, total_reward, smooth_reward.value, different_actions, most_frequent, steps
            # ))


if __name__ == '__main__':
    env = GridParam(SIZE)
    learn(env)
