""" Actor Critic.
    Simple version

    Roadmap:
        - Target network
        - Experience replay
"""
import keras.backend as K
import numpy as np
from gym import Env
from keras import Sequential, Input, Model
from keras.layers import Dense, Lambda
from keras.optimizers import Adam

from environments.grid_param import GridParam

DISCOUNT = .99
ACTOR_LR = 0.001
CRITIC_LR = 0.01

SIZE = 4


def cross_entropy(y_true, y_pred):
    return -y_true * K.log(y_pred)


class Actor:
    MAX_STD = 1. / 2

    def __init__(self, n, lr):
        """

        :param n: Size of the board
        :param lr: Learning rate of the actor
        """
        self.lr = lr
        self.n = n
        self._build()

    def _build(self):
        state = Input(shape=(4,))
        action = Input(shape=(2,))

        hidden = Dense(20, activation='relu')(state)
        mean_ = Dense(2)(hidden)
        std_ = Dense(2)(hidden)

        mean = Lambda(lambda x: K.clip(x, 0., self.n - 1))(mean_)
        std = Lambda(lambda x: K.sigmoid(x) * self.n * Actor.MAX_STD)(std_)

        action_proba = Lambda(self._build_proba)([mean, std, action])
        # entropy = self._build_entropy(x_dist, y_dist)

        self.policy_model = Model(inputs=[state], outputs=[mean, std])
        self.proba_model = Model(inputs=[state, action], outputs=[action_proba])

        self.opt = Adam(lr=self.lr)
        self.proba_model.compile(optimizer=self.opt, loss=cross_entropy)

    def _build_proba(self, data):
        mean, std, action = data
        lo = K.clip(mean - std, 0., self.n - 1)
        hi = K.clip(mean + std, 0., self.n - 1)
        diff = hi - lo
        prod = K.prod(diff, 1, keepdims=True)
        proba = 1. / (prod + 1e-10)
        return proba

    # @staticmethod
    # def _build_entropy(x_dist, y_dist):
    #     return 1

    def choose_action(self, s):
        mean, std = self.policy_model.predict(s)

        if np.any(np.isnan(mean)):
            print(self.policy_model.get_weights())
            print(s)
            raise ValueError("{} - {}".format(mean, std))

        lo = np.clip(mean - std, 0., self.n - 1)
        hi = np.clip(mean + std, 0., self.n - 1)
        points = np.random.random((1, 2))
        points *= hi - lo
        points += lo
        points = np.round(points).astype(np.int).flatten()

        # print(mean, std, points)

        try:
            assert (0 <= np.min(points) and np.max(points) <= self.n - 1)
        except AssertionError:
            print(points, lo, hi, mean, std)

        return tuple(points.tolist())

    def learn(self, state, action, td_error):
        error = np.array([td_error]).reshape(1, 1)
        self.proba_model.train_on_batch([state, action], error)

    def save(self, path):
        self.proba_model.save_weights(path)

    def load(self, path):
        self.proba_model.load_weights(path)


class Critic:
    def __init__(self, lr):
        self.model = Sequential()
        self.model.add(Dense(20,
                             activation='relu',
                             input_shape=(4,)))
        self.model.add(Dense(1))
        self.opt = Adam(lr=lr)
        self.model.compile(optimizer=self.opt, loss='mse')

    def learn(self, s, r, s_):
        ss = np.concatenate([s, s_]).reshape(2, -1)
        values = self.model.predict(ss)
        vs = values[:1]
        vs_ = values[1:]
        t = r + DISCOUNT * vs_
        td_error = t - vs

        self.model.train_on_batch(s.reshape(1, -1), t)
        return td_error

    def save(self, path):
        self.model.save_weights(path)


def tonp(data):
    return np.array(data).reshape(1, -1)


class SmoothValue:
    def __init__(self):
        self._value = None

    def update(self, x):
        if self._value is None:
            self._value = x
        else:
            self._value = .99 * self._value + .01 * x

    @property
    def value(self):
        return self._value


def learn(env: Env):
    actor = Actor(SIZE, lr=ACTOR_LR)
    critic = Critic(lr=CRITIC_LR)

    RENDER = False
    SAVE = False

    episode = 0
    running_reward = SmoothValue()

    # Keep training forever
    while True:
        state = env.reset()
        state = tonp(state)

        track_r = 0
        done = False

        while not done:
            if RENDER:
                env.render()

            action = actor.choose_action(state)
            next_state, reward, done, _ = env.step(action)

            track_r += reward

            next_state, reward, action = map(tonp, [next_state, reward, action])

            td_error = critic.learn(state, reward, next_state)
            actor.learn(state, action, td_error)

            state = next_state

        running_reward.update(track_r)

        episode += 1
        print("episode:", episode, "  reward:", running_reward.value)

        if SAVE and episode % 100 == 0:
            actor.save("models/ac0-actor-{}.nn".format(episode))
            critic.save("models/ac0-critic-{}.nn".format(episode))


if __name__ == '__main__':
    grid = GridParam(SIZE)
    learn(grid)
