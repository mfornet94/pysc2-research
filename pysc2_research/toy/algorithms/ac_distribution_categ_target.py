"""

    Statistics:


Checkpoint: 700

STATS_EVERY          100
DISCOUNT             0.99
ACTOR_LR             0.01
CRITIC_LR            0.05
SIZE                 4
OPTIMAL_STRATEGY     False
REGULARIZATION       True

E   100 26.30138053418243
E   200 37.43247819524456
E   300 43.62492897549678
E   400 45.65558674160371
E   500 50.635019911857206
E   600 53.02866920306056
E   700 54.30210532669134


Checkpoint: 400

STATS_EVERY          100
DISCOUNT             0.99
ACTOR_LR             0.01
CRITIC_LR            0.05
SIZE                 4
OPTIMAL_STRATEGY     False
REGULARIZATION       True
ENTROPY_MULT         0.001

E   100 29.113182590848297
E   200 46.64236482237398
E   300 54.67660339650036
E   400 54.48391479687123

Checkpoint: 600

RENDER               False
STATS_EVERY          100
DISCOUNT             0.99
ACTOR_LR             0.001
CRITIC_LR            0.005
SIZE                 4
OPTIMAL_STRATEGY     False
REGULARIZATION       0.01
ENTROPY_MULT         0.001
EXPERIENCE_REPLAY    True
ALPHA_ACTOR          0.001
ALPHA_CRITIC         0.005

E   100 9.586999773161136
E   200 38.26553279577461
E   300 66.04347534758362
E   400 78.8488352697621
E   500 85.34520093257909
E   600 87.76784631354548

Checkpoint: 500

STATS_EVERY          100
DISCOUNT             0.99
ACTOR_LR             0.01
CRITIC_LR            0.05
SIZE                 8
OPTIMAL_STRATEGY     False
REGULARIZATION       True
ENTROPY_MULT         0.001

E   100 2.4988640665136597
E   200 2.9089701545035873
E   300 2.6679388455911326
E   400 3.0717998246431355
E   500 3.0887731701091656

Checkpoint: 700

(Bigger Model)

STATS_EVERY          100
DISCOUNT             0.99
ACTOR_LR             0.01
CRITIC_LR            0.05
SIZE                 8
OPTIMAL_STRATEGY     False
REGULARIZATION       True
ENTROPY_MULT         0.001

E   100 1.9745655030825706
E   200 2.797609269945293
E   300 3.0675161243283147
E   400 3.447189082785889
E   500 3.250849135263938
E   600 2.9215725399184214
E   700 3.1511025244100943
"""
import keras.backend as K
import numpy as np
from gym import Env
from keras import Sequential, Input, Model
from keras.layers import Dense, Lambda, Reshape, Concatenate, Add
from keras.optimizers import Adam
from keras.regularizers import l2
from keras.utils import to_categorical

from toy.buffer import NoBuffer, CyclicBuffer
from environments.grid_param import GridParam

RENDER = False
STATS_EVERY = 100

# Training parameters
DISCOUNT = .99
ACTOR_LR = 0.001
CRITIC_LR = 0.005

# Environment Control
SIZE = 8
OPTIMAL_STRATEGY = False

REGULARIZATION = 0.01
ENTROPY_MULT = 0.001

EXPERIENCE_REPLAY = True
ALPHA_ACTOR = 0.001
ALPHA_CRITIC = 0.005


def cross_entropy(y_true, y_pred):
    return -y_true * K.log(y_pred)


def maximize(y_true, y_pred):
    return -y_true * y_pred


class Actor:
    MAX_STD = 1. / 2

    def __init__(self, n, lr):
        """

        :param n: Size of the board
        :param lr: Learning rate of the actor
        """
        self.lr = lr
        self.n = n
        self._build()

    def _build(self):
        state = Input(shape=(4,))
        action = Input(shape=(2, self.n))

        reg = l2(REGULARIZATION)

        hidden = Dense(self.n**2, activation='relu', kernel_regularizer=reg, bias_regularizer=reg)(state)
        # hidden = Dense(20, activation='relu', kernel_regularizer=reg, bias_regularizer=reg)(hidden)
        x_proba = Dense(self.n, activation='softmax', kernel_regularizer=reg, bias_regularizer=reg)(hidden)
        y_proba = Dense(self.n, activation='softmax', kernel_regularizer=reg)(hidden)

        action_proba = Lambda(self._build_proba)([x_proba, y_proba, action])

        entropy = self._build_entropy(x_proba, y_proba)
        # print(entropy)

        self.policy_model = Model(inputs=state, outputs=[x_proba, y_proba])
        self.proba_model = Model(inputs=[state, action], outputs=[action_proba, entropy])

        self.opt = Adam(lr=self.lr)
        self.proba_model.compile(optimizer=self.opt, loss=[cross_entropy, maximize])

    def _build_proba(self, data):
        x_proba, y_proba, categ_action = data
        x_proba = Reshape((1, self.n))(x_proba)
        y_proba = Reshape((1, self.n))(y_proba)
        coord_proba = Concatenate(1)([x_proba, y_proba])
        axis_prob = K.sum(categ_action * coord_proba, 2)
        return K.prod(axis_prob, 1, keepdims=True) + 1e-10

    def _build_entropy(self, x_proba, y_proba):
        x_entropy = Lambda(self._entropy)(x_proba)
        y_entropy = Lambda(self._entropy)(y_proba)
        return Add()([x_entropy, y_entropy])

    @staticmethod
    def _entropy(dist):
        return K.sum(-dist * K.log(dist + 1e-10), -1, keepdims=True)

    def choose_action(self, s):
        if OPTIMAL_STRATEGY:
            x, y = s.flatten()[-2:]
            return int(x), int(y)
        else:
            x_proba, y_proba = self.policy_model.predict(s)

            # try:
            #     assert x_proba.min() >= 0.
            #     assert y_proba.min() >= 0.
            #     assert np.allclose(x_proba.sum(), 1.)
            #     assert np.allclose(y_proba.sum(), 1.)
            # except AssertionError:
            #     print(x_proba)
            #     print(y_proba)
            #     raise ValueError()

            x = np.random.choice(self.n, p=x_proba.flatten())
            y = np.random.choice(self.n, p=y_proba.flatten())
            return x, y

    def learn(self, state, action, td_error):
        action = to_categorical(action, self.n)
        # action_proba, entropy = self.proba_model.predict([state, action])

        entropy_mult = np.full((state.shape[0],), ENTROPY_MULT)
        self.proba_model.train_on_batch([state, action], [td_error, entropy_mult])

    def save(self, path):
        self.proba_model.save_weights(path)

    def load(self, path):
        self.proba_model.load_weights(path)

    def get_weights(self):
        return self.proba_model.get_weights()

    def set_weights(self, weights):
        self.proba_model.set_weights(weights)


class Critic:
    def __init__(self, lr):
        self.model = Sequential()
        self.model.add(Dense(20, activation='relu', input_shape=(4,)))
        # self.model.add(Dense(20, activation='relu'))
        self.model.add(Dense(1))
        self.opt = Adam(lr=lr)
        self.model.compile(optimizer=self.opt, loss='mse')

    def get_error(self, s, r, s_):
        vs = self.model.predict(s)
        vs_ = self.model.predict(s_)
        t = r + DISCOUNT * vs_
        td_error = t - vs
        return td_error

    def learn(self, s, r, s_):
        vs_ = self.model.predict(s_)
        t = r + DISCOUNT * vs_
        self.model.train_on_batch(s.reshape(-1, 4), t)

    def save(self, path):
        self.model.save_weights(path)

    def get_weights(self):
        return self.model.get_weights()

    def set_weights(self, weights):
        self.model.set_weights(weights)


def tonp(data):
    return np.array(data).reshape(1, -1)


class SmoothValue:
    def __init__(self):
        self._value = None

    def update(self, x):
        if self._value is None:
            self._value = x
        else:
            self._value = .99 * self._value + .01 * x

    @property
    def value(self):
        return self._value


class Brain:
    def __init__(self):
        self.actor = Actor(SIZE, lr=ACTOR_LR)
        self.critic = Critic(lr=CRITIC_LR)

        self.actor_target = Actor(SIZE, lr=ACTOR_LR)
        self.actor_target.set_weights(self.actor.get_weights())

        self.critic_target = Critic(lr=CRITIC_LR)
        self.critic_target.set_weights(self.critic.get_weights())

    def choose_action(self, state):
        return self.actor_target.choose_action(state)

    def critic_learn(self, state, reward, next_state):
        vs = self.critic.model.predict(state)
        vs_ = self.critic_target.model.predict(next_state)
        t = reward + DISCOUNT * vs_
        td_error = t - vs

        self.critic.model.train_on_batch(state.reshape(-1, 4), t)

        critic_weights = self.critic.get_weights()
        critic_target_weights = self.critic_target.get_weights()

        params = []
        for wa, wat in zip(critic_weights, critic_target_weights):
            nw = ALPHA_CRITIC * wa + (1. - ALPHA_CRITIC) * wat
            params.append(nw)

        self.critic_target.set_weights(params)

        return td_error

    def actor_learn(self, state, action, td_error):
        self.actor.learn(state, action, td_error)

        actor_weights = self.actor.get_weights()
        actor_target_weights = self.actor_target.get_weights()

        params = []
        for wa, wat in zip(actor_weights, actor_target_weights):
            nw = ALPHA_ACTOR * wa + (1. - ALPHA_ACTOR) * wat
            params.append(nw)

        self.actor_target.set_weights(params)


def show_stats(checkpointed_rewards):
    episode = STATS_EVERY * len(checkpointed_rewards)
    rewards = '\n'.join('E {:>5} {}'.format(100 * (ep + 1), rew) for ep, rew in enumerate(checkpointed_rewards))
    g = {k: v for (k, v) in globals().items() if k.isupper()}
    g.pop('K')
    params = '\n'.join('{:<20} {}'.format(k, v) for k, v in g.items())
    print("""
Checkpoint: {episode}

{params}

{rewards}
""".format(episode=episode, params=params, rewards=rewards))


def learn(env: Env):
    buffer = (CyclicBuffer if EXPERIENCE_REPLAY else NoBuffer)()
    buffer._capacity = 10 ** 6

    brain = Brain()

    episode = 0
    running_reward = SmoothValue()

    checkpointed_rewards = []

    # Keep training forever
    while True:
        state = env.reset()
        state = tonp(state)

        track_r = 0
        done = False

        while not done:
            if RENDER:
                env.render()

            action = brain.choose_action(state)
            next_state, reward, done, _ = env.step(action)

            track_r += reward

            next_state, reward, action = map(tonp, [next_state, reward, action])

            buffer.update(state, action, reward, next_state)
            s, a, r, ns = buffer.sample()
            # print(s, a, r, ns)

            td_error = brain.critic_learn(s, r, ns)
            brain.actor_learn(s, a, td_error)

            state = next_state

        running_reward.update(track_r)

        episode += 1
        print("episode:", episode, "  reward:", running_reward.value, "  buffer:", buffer._size)

        if episode % STATS_EVERY == 0:
            checkpointed_rewards.append(running_reward.value)
            show_stats(checkpointed_rewards)


if __name__ == '__main__':
    grid = GridParam(SIZE)
    learn(grid)
