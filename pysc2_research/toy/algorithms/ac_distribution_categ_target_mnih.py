"""

    Statistics:


Checkpoint: 700

STATS_EVERY          100
DISCOUNT             0.99
ACTOR_LR             0.01
CRITIC_LR            0.05
SIZE                 4
OPTIMAL_STRATEGY     False
REGULARIZATION       True

E   100 26.30138053418243
E   200 37.43247819524456
E   300 43.62492897549678
E   400 45.65558674160371
E   500 50.635019911857206
E   600 53.02866920306056
E   700 54.30210532669134


Checkpoint: 400

STATS_EVERY          100
DISCOUNT             0.99
ACTOR_LR             0.01
CRITIC_LR            0.05
SIZE                 4
OPTIMAL_STRATEGY     False
REGULARIZATION       True
ENTROPY_MULT         0.001

E   100 29.113182590848297
E   200 46.64236482237398
E   300 54.67660339650036
E   400 54.48391479687123

Checkpoint: 600

RENDER               False
STATS_EVERY          100
DISCOUNT             0.99
ACTOR_LR             0.001
CRITIC_LR            0.005
SIZE                 4
OPTIMAL_STRATEGY     False
REGULARIZATION       0.01
ENTROPY_MULT         0.001
EXPERIENCE_REPLAY    True
ALPHA_ACTOR          0.001
ALPHA_CRITIC         0.005

E   100 9.586999773161136
E   200 38.26553279577461
E   300 66.04347534758362
E   400 78.8488352697621
E   500 85.34520093257909
E   600 87.76784631354548

Checkpoint: 1600

RENDER               False
STATS_EVERY          100
DISCOUNT             0.99
LEARNING_RATE        0.00025
ACTOR_LR             0.00025
CRITIC_LR            0.00125
OPTIMIZER            <class 'keras.optimizers.RMSprop'>
SIZE                 4
OPTIMAL_STRATEGY     False
REGULARIZATION       0.01
ENTROPY_INIT         1.0
ENTROPY_END          0.001
ENTROPY_TIME         1000000
EXPERIENCE_REPLAY    True
CONTINUOUS_UPDATE    False
ALPHA_ACTOR          0.001
ALPHA_CRITIC         0.005
UPDATE_ACTOR_EVERY   20000
UPDATE_CRITIC_EVERY  4000
BUFFER_CAPACITY      100000
BATCH_SIZE           16
LEARN_EVERY          4

E   100 10.09101246807674
E   200 8.017484140857015
E   300 7.588868952304648
E   400 7.443484439891691
E   500 7.155704019186401
E   600 6.910927290923452
E   700 9.007749465290885
E   800 11.531652787043875
E   900 12.143923133083643
E  1000 12.714478966848548
E  1100 13.158052933956538
E  1200 13.049186385670529
E  1300 13.24577642565517
E  1400 14.279240046949832
E  1500 14.730222552060162
E  1600 15.015693609698147

Checkpoint: 4800

RENDER               False
STATS_EVERY          100
DISCOUNT             0.99
LEARNING_RATE        0.001
ACTOR_LR             0.001
CRITIC_LR            0.005
OPTIMIZER            <class 'keras.optimizers.RMSprop'>
SIZE                 4
OPTIMAL_STRATEGY     False
REGULARIZATION       0.01
ENTROPY_INIT         0.001
ENTROPY_END          0.001
ENTROPY_TIME         1
EXPERIENCE_REPLAY    True
CONTINUOUS_UPDATE    True
ALPHA_ACTOR          1.0
ALPHA_CRITIC         1.0
UPDATE_ACTOR_EVERY   20000
UPDATE_CRITIC_EVERY  4000
BUFFER_CAPACITY      1000000
BATCH_SIZE           32
LEARN_EVERY          1

E   100 13.380886190007786
E   200 18.99634595263899
E   300 30.89112946191929
E   400 49.840387549267824
E   500 54.27535528799955
E   600 62.428556096935615
E   700 65.69880686302528
E   800 70.94441740188572
E   900 72.80863691370064
E  1000 73.40283503746336
E  1100 70.4981273025715
E  1200 71.2440941732026
E  1300 73.3855116797947
E  1400 75.01168182436281
E  1500 71.68979185688704
E  1600 72.2438408111976
E  1700 60.69607698393984
E  1800 71.13245747213115
E  1900 67.06092531390446
E  2000 75.70481868917224
E  2100 56.14232754113696
E  2200 61.17823018965101
E  2300 67.00619935058894
E  2400 69.26332314008364
E  2500 80.6853747461305
E  2600 88.64508284082348
E  2700 92.33873804377912
E  2800 92.32582527126924
E  2900 91.55559869869494
E  3000 87.57971579911643
E  3100 87.01308211139558
E  3200 87.22942942934824
E  3300 89.44313342873731
E  3400 89.53909380152875
E  3500 87.91885631868657
E  3600 88.44973597610928
E  3700 88.90106905211262
E  3800 89.04286750037805
E  3900 89.31753312800775
E  4000 88.25848079988673
E  4100 89.06530050586825
E  4200 89.3198075172838
E  4300 88.03187059484594
E  4400 83.12659485414372
E  4500 79.2696912542181
E  4600 81.81585835768051
E  4700 86.16698935385304
E  4800 89.38195872760636

Checkpoint: 500

STATS_EVERY          100
DISCOUNT             0.99
ACTOR_LR             0.01
CRITIC_LR            0.05
SIZE                 8
OPTIMAL_STRATEGY     False
REGULARIZATION       True
ENTROPY_MULT         0.001

E   100 2.4988640665136597
E   200 2.9089701545035873
E   300 2.6679388455911326
E   400 3.0717998246431355
E   500 3.0887731701091656

Checkpoint: 700

(Bigger Model)

STATS_EVERY          100
DISCOUNT             0.99
ACTOR_LR             0.01
CRITIC_LR            0.05
SIZE                 8
OPTIMAL_STRATEGY     False
REGULARIZATION       True
ENTROPY_MULT         0.001

E   100 1.9745655030825706
E   200 2.797609269945293
E   300 3.0675161243283147
E   400 3.447189082785889
E   500 3.250849135263938
E   600 2.9215725399184214
E   700 3.1511025244100943

Checkpoint: 1200

RENDER               False
STATS_EVERY          100
DISCOUNT             0.99
ACTOR_LR             0.001
CRITIC_LR            0.005
SIZE                 8
OPTIMAL_STRATEGY     False
REGULARIZATION       0.01
ENTROPY_MULT         0.001
EXPERIENCE_REPLAY    True
ALPHA_ACTOR          0.001
ALPHA_CRITIC         0.005

E   100 0.7052226614203723
E   200 1.014325441600884
E   300 1.2055149763326234
E   400 1.3470661835859705
E   500 1.3584548792549134
E   600 1.669220840665965
E   700 1.8980737154349732
E   800 1.8570602410195087
E   900 1.9632045297684757
E  1000 1.9504195711196968
E  1100 1.7805357909543371
E  1200 1.5996335516709228

Checkpoint: 300

RENDER               False
STATS_EVERY          100
DISCOUNT             0.99
LEARNING_RATE        0.00025
ACTOR_LR             0.00025
CRITIC_LR            0.00125
OPTIMIZER            <class 'keras.optimizers.RMSprop'>
SIZE                 8
OPTIMAL_STRATEGY     False
REGULARIZATION       0.01
ENTROPY_INIT         1.0
ENTROPY_END          0.0001
ENTROPY_TIME         100000
EXPERIENCE_REPLAY    True
CONTINUOUS_UPDATE    False
ALPHA_ACTOR          0.001
ALPHA_CRITIC         0.005
UPDATE_EVERY         10000

E   100 2.1552383513772075
E   200 1.872880984903174
E   300 1.8758631127398318

Checkpoint: 1000

RENDER               False
STATS_EVERY          100
DISCOUNT             0.99
LEARNING_RATE        0.00025
ACTOR_LR             0.00025
CRITIC_LR            0.00125
OPTIMIZER            <class 'keras.optimizers.RMSprop'>
SIZE                 8
OPTIMAL_STRATEGY     False
REGULARIZATION       0.01
ENTROPY_INIT         1.0
ENTROPY_END          0.001
ENTROPY_TIME         1000000
EXPERIENCE_REPLAY    True
CONTINUOUS_UPDATE    False
ALPHA_ACTOR          0.001
ALPHA_CRITIC         0.005
UPDATE_ACTOR_EVERY   5000
UPDATE_CRITIC_EVERY  1000

E   100 2.7598565910060793
E   200 2.938874200870173
E   300 3.0090399970787653
E   400 3.211888444159893
E   500 2.9331235352264025
E   600 2.8635575235016946
E   700 2.895214076494503
E   800 3.073624403317556
E   900 3.003216115469694
E  1000 3.1250684364776333

"""
import keras.backend as K
import numpy as np
from gym import Env
from keras import Sequential, Input, Model
from keras.layers import Dense, Lambda, Reshape, Concatenate, Add
from keras.optimizers import RMSprop
from keras.regularizers import l2
from keras.utils import to_categorical

from toy.buffer import NoBuffer, CyclicBuffer
from environments.grid_param import GridParam

RENDER = False
STATS_EVERY = 100

# Training parameters
DISCOUNT = .99

LEARNING_RATE = .001
ACTOR_LR = LEARNING_RATE
CRITIC_LR = LEARNING_RATE * 5

OPTIMIZER = RMSprop

# Environment Control
SIZE = 4
OPTIMAL_STRATEGY = False

REGULARIZATION = 0.01

ENTROPY_INIT = 0.001
ENTROPY_END = 0.001
ENTROPY_TIME = 1

EXPERIENCE_REPLAY = True

CONTINUOUS_UPDATE = True

ALPHA_ACTOR = 0.0001
ALPHA_CRITIC = 0.001

UPDATE_ACTOR_EVERY = 5000 * 4
UPDATE_CRITIC_EVERY = 1000 * 4

BUFFER_CAPACITY = 10 ** 6
BATCH_SIZE = 32

LEARN_EVERY = 1


def cross_entropy(y_true, y_pred):
    return -y_true * K.log(y_pred)


def maximize(y_true, y_pred):
    return -y_true * y_pred


class Actor:
    MAX_STD = 1. / 2

    def __init__(self, n, lr):
        """

        :param n: Size of the board
        :param lr: Learning rate of the actor
        """
        self.lr = lr
        self.n = n
        self._build()

    def _build(self):
        state = Input(shape=(4,))
        action = Input(shape=(2, self.n))

        reg = l2(REGULARIZATION)

        hidden = Dense(20, activation='relu', kernel_regularizer=reg, bias_regularizer=reg)(state)
        # hidden = Dense(20, activation='relu', kernel_regularizer=reg, bias_regularizer=reg)(hidden)
        x_proba = Dense(self.n, activation='softmax', kernel_regularizer=reg, bias_regularizer=reg)(hidden)
        y_proba = Dense(self.n, activation='softmax', kernel_regularizer=reg, bias_regularizer=reg)(hidden)

        action_proba = Lambda(self._build_proba)([x_proba, y_proba, action])

        entropy = self._build_entropy(x_proba, y_proba)
        # print(entropy)

        self.policy_model = Model(inputs=state, outputs=[x_proba, y_proba])
        self.proba_model = Model(inputs=[state, action], outputs=[action_proba, entropy])

        self.opt = OPTIMIZER(lr=self.lr)
        self.proba_model.compile(optimizer=self.opt, loss=[cross_entropy, maximize])

    def _build_proba(self, data):
        x_proba, y_proba, categ_action = data
        x_proba = Reshape((1, self.n))(x_proba)
        y_proba = Reshape((1, self.n))(y_proba)
        coord_proba = Concatenate(1)([x_proba, y_proba])
        axis_prob = K.sum(categ_action * coord_proba, 2)
        return K.prod(axis_prob, 1, keepdims=True) + 1e-10

    def _build_entropy(self, x_proba, y_proba):
        x_entropy = Lambda(self._entropy)(x_proba)
        y_entropy = Lambda(self._entropy)(y_proba)
        return Add()([x_entropy, y_entropy])

    @staticmethod
    def _entropy(dist):
        return K.sum(-dist * K.log(dist + 1e-10), -1, keepdims=True)

    def choose_action(self, s):
        if OPTIMAL_STRATEGY:
            x, y = s.flatten()[-2:]
            return int(x), int(y)
        else:
            x_proba, y_proba = self.policy_model.predict(s)

            # try:
            #     assert x_proba.min() >= 0.
            #     assert y_proba.min() >= 0.
            #     assert np.allclose(x_proba.sum(), 1.)
            #     assert np.allclose(y_proba.sum(), 1.)
            # except AssertionError:
            #     print(x_proba)
            #     print(y_proba)
            #     raise ValueError()

            x = np.random.choice(self.n, p=x_proba.flatten())
            y = np.random.choice(self.n, p=y_proba.flatten())
            return x, y

    def learn(self, state, action, td_error, entropy_mult):
        action = to_categorical(action, self.n)
        # action_proba, entropy = self.proba_model.predict([state, action])

        entropy_mult = np.full((state.shape[0],), entropy_mult)
        self.proba_model.train_on_batch([state, action], [td_error, entropy_mult])

    def save(self, path):
        self.proba_model.save_weights(path)

    def load(self, path):
        self.proba_model.load_weights(path)

    def get_weights(self):
        return self.proba_model.get_weights()

    def set_weights(self, weights):
        self.proba_model.set_weights(weights)


class Critic:
    def __init__(self, lr):
        self.model = Sequential()
        self.model.add(Dense(20, activation='relu', input_shape=(4,)))
        # self.model.add(Dense(20, activation='relu'))
        self.model.add(Dense(1))
        self.opt = OPTIMIZER(lr=lr)
        self.model.compile(optimizer=self.opt, loss='mse')

    def get_error(self, s, r, s_):
        vs = self.model.predict(s)
        vs_ = self.model.predict(s_)
        t = r + DISCOUNT * vs_
        td_error = t - vs
        return td_error

    def learn(self, s, r, s_):
        vs_ = self.model.predict(s_)
        t = r + DISCOUNT * vs_
        self.model.train_on_batch(s.reshape(-1, 4), t)

    def save(self, path):
        self.model.save_weights(path)

    def get_weights(self):
        return self.model.get_weights()

    def set_weights(self, weights):
        self.model.set_weights(weights)


def tonp(data):
    return np.array(data).reshape(1, -1)


class SmoothValue:
    def __init__(self):
        self._value = None

    def update(self, x):
        if self._value is None:
            self._value = x
        else:
            self._value = .99 * self._value + .01 * x

    @property
    def value(self):
        return self._value


class Schedule:
    def __init__(self, init_value, final_value, steps):
        self.init_value = np.log(init_value)
        self.final_value = np.log(final_value)
        self.steps = steps

    def value(self, t):
        t = min(t, self.steps)
        alpha = t / self.steps
        x = (1. - alpha) * self.init_value + alpha * self.final_value
        return np.exp(x)


class Brain:
    def __init__(self):
        self.actor = Actor(SIZE, lr=ACTOR_LR)
        self.critic = Critic(lr=CRITIC_LR)

        self.actor_target = Actor(SIZE, lr=ACTOR_LR)
        self.actor_target.set_weights(self.actor.get_weights())

        self.critic_target = Critic(lr=CRITIC_LR)
        self.critic_target.set_weights(self.critic.get_weights())

        self.entropy_schedule = Schedule(ENTROPY_INIT, ENTROPY_END, ENTROPY_TIME)

        self.steps = 0

    def choose_action(self, state):
        return self.actor_target.choose_action(state)

    def critic_learn(self, state, reward, next_state):
        self.steps += 1

        vs = self.critic.model.predict(state)
        vs_ = self.critic_target.model.predict(next_state)
        t = reward + DISCOUNT * vs_
        td_error = t - vs

        self.critic.model.train_on_batch(state.reshape(-1, 4), t)

        if CONTINUOUS_UPDATE:
            critic_weights = self.critic.get_weights()
            critic_target_weights = self.critic_target.get_weights()

            params = []
            for wa, wat in zip(critic_weights, critic_target_weights):
                nw = ALPHA_CRITIC * wa + (1. - ALPHA_CRITIC) * wat
                params.append(nw)

            self.critic_target.set_weights(params)

        else:
            if self.steps % UPDATE_CRITIC_EVERY == 0:
                print("UPDATE CRITIC")
                self.critic_target.set_weights(self.critic.get_weights())

        return td_error

    def actor_learn(self, state, action, td_error):
        pass
        self.actor.learn(state, action, td_error, self.entropy_schedule.value(self.steps))

        if CONTINUOUS_UPDATE:
            actor_weights = self.actor.get_weights()
            actor_target_weights = self.actor_target.get_weights()

            params = []
            for wa, wat in zip(actor_weights, actor_target_weights):
                nw = ALPHA_ACTOR * wa + (1. - ALPHA_ACTOR) * wat
                params.append(nw)

            self.actor_target.set_weights(params)
        else:
            if self.steps % UPDATE_ACTOR_EVERY == 0:
                print("UPDATE ACTOR")
                self.actor_target.set_weights(self.actor.get_weights())


def show_stats(checkpointed_rewards):
    episode = STATS_EVERY * len(checkpointed_rewards)
    rewards = '\n'.join('E {:>5} {}'.format(100 * (ep + 1), rew) for ep, rew in enumerate(checkpointed_rewards))
    g = {k: v for (k, v) in globals().items() if k.isupper()}
    g.pop('K')
    params = '\n'.join('{:<20} {}'.format(k, v) for k, v in g.items())
    print("""
Checkpoint: {episode}

{params}

{rewards}
""".format(episode=episode, params=params, rewards=rewards))


def learn(env: Env):
    buffer = (CyclicBuffer if EXPERIENCE_REPLAY else NoBuffer)()
    buffer._capacity = BUFFER_CAPACITY
    buffer._batch_size = BATCH_SIZE

    brain = Brain()

    episode = 0
    running_reward = SmoothValue()

    checkpointed_rewards = []

    # Keep training forever
    while True:
        state = env.reset()
        state = tonp(state)

        track_r = 0
        done = False

        step = 0

        while not done:
            step += 1

            if RENDER:
                env.render()

            action = brain.choose_action(state)
            next_state, reward, done, _ = env.step(action)

            track_r += reward

            next_state, reward, action = map(tonp, [next_state, reward, action])

            buffer.update(state, action, reward, next_state)

            if step % LEARN_EVERY == 0:
                s, a, r, ns = buffer.sample()
                td_error = brain.critic_learn(s, r, ns)
                brain.actor_learn(s, a, td_error)

            state = next_state

        running_reward.update(track_r)

        episode += 1
        print("episode:", episode, "  reward:", running_reward.value, "  buffer:", buffer._size)

        if episode % STATS_EVERY == 0:
            checkpointed_rewards.append(running_reward.value)
            show_stats(checkpointed_rewards)


if __name__ == '__main__':
    grid = GridParam(SIZE)
    learn(grid)
