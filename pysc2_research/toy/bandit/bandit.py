import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def generate_bandits(n):
    return np.random.randn(n)


class Bandit:
    def __init__(self, n):
        self._n = n
        self.bandits = generate_bandits(n)

    def reset(self):
        self.bandits = generate_bandits(self._n)

    def sample(self, a):
        x = np.random.randn() + self.bandits[a]
        return x

    @property
    def best(self):
        return np.argmax(self.bandits)


class Actor:
    def __init__(self, n, q=0., eps=0., mode='epsilon'):
        self.n = n
        self._q = q
        self.eps = eps
        self.mode = mode

        self.reset()

    def reset(self):
        self.value = np.full(self.n, self._q)
        self._rewards = [[] for _ in range(self.n)]

    def get(self):
        if self.mode == 'epsilon':
            if np.random.random() < self.eps:
                return np.random.randint(self.n)
            else:
                return np.argmax(self.value)
        elif self.mode == 'softmax':
            e = np.exp(np.array(self.value) * 100)
            e /= e.sum()
            return np.random.choice(self.n, p=e)

    def feedback(self, a, r):
        self._rewards[a].append(r)
        self.value[a] = np.mean(self._rewards[a])


def run(bandit, actor, steps):
    total_reward = 0.
    total_best = 0.

    actor.reset()
    bandit.reset()

    average_reward_vec = []
    average_best_vec = []

    for step in range(steps):
        action = actor.get()
        reward = bandit.sample(action)
        actor.feedback(action, reward)

        total_reward += reward
        total_best += int(action == bandit.best)

        average_reward_vec.append(total_reward / (step + 1))
        average_best_vec.append(total_best / (step + 1))

    return np.array(average_reward_vec), np.array(average_best_vec) * 100


class Display:
    def __init__(self):
        fig = plt.figure()
        self.rew_plt = fig.add_subplot(211)
        self.best_plt = fig.add_subplot(212)

    def add(self, reward, best, name):
        self.rew_plt.plot(reward, label=name)
        self.best_plt.plot(best, label=name)

    def show(self):
        plt.legend()
        plt.show()


def run_experiment(display, n, q, eps, trials, steps, name, mode='epsilon'):
    actor = Actor(n, q, eps, mode)
    bandit = Bandit(n)

    tot_reward = np.zeros(steps)
    tot_best = np.zeros(steps)

    for _ in tqdm(range(trials)):
        reward, best = run(bandit, actor, steps)
        tot_reward += reward
        tot_best += best

    tot_reward /= trials
    tot_best /= trials

    display.add(tot_reward, tot_best, name)


def main():
    display = Display()

    trials = 20
    steps = 2000

    run_experiment(display, n=10, q=0., eps=0., trials=trials, steps=steps, name="greedy")
    run_experiment(display, n=10, q=5., eps=0., trials=trials, steps=steps, name="optimistic")
    run_experiment(display, n=10, q=0., eps=0.5, trials=trials, steps=steps, name="epsilon-0.5")
    run_experiment(display, n=10, q=0., eps=0.1, trials=trials, steps=steps, name="epsilon-0.1")
    run_experiment(display, n=10, q=0., eps=0.01, trials=trials, steps=steps, name="epsilon-0.01")
    run_experiment(display, n=10, q=0., eps=0.1, trials=trials, steps=steps, name="softmax", mode='softmax')
    run_experiment(display, n=10, q=5., eps=0.1, trials=trials, steps=steps, name="optimistic-softmax", mode='softmax')
    # run_experiment(display, n=10, q=0., eps=0.01, trials=trials, steps=steps, name="epsilon-0.01")

    display.show()


if __name__ == '__main__':
    main()
