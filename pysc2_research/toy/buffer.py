import numpy as np


class BaseBuffer:
    def update(self, state, action, reward, next_state):
        raise NotImplementedError()

    def sample(self):
        raise NotImplementedError()


class NoBuffer(BaseBuffer):
    def update(self, state, action, reward, next_state):
        self.last = (state, action, reward, next_state)

    def sample(self):
        return self.last


class CyclicBuffer(BaseBuffer):
    def __init__(self, capacity=10000, batch_size=32):
        self._pointer = 0
        self._size = 0
        self._capacity = capacity
        self._buffer = None
        self._batch_size = batch_size

    def _build(self, state, action, reward):
        state_shape = state.shape
        action_shape = action.shape
        reward_shape = reward.shape
        self._buffer = [
            np.zeros((self._capacity, *state_shape[1:])),
            np.zeros((self._capacity, *action_shape[1:])),
            np.zeros((self._capacity, *reward_shape[1:])),
            np.zeros((self._capacity, *state_shape[1:])),
        ]

    def update(self, state, action, reward, next_state):
        if self._buffer is None:
            self._build(state, action, reward)

        pnt = self._pointer
        self._buffer[0][pnt] = state
        self._buffer[1][pnt] = action
        self._buffer[2][pnt] = reward
        self._buffer[3][pnt] = next_state

        self._pointer += 1
        self._size = min(self._size + 1, self._capacity)
        if self._pointer == self._capacity:
            self._pointer = 0

    def sample(self):
        size = min(self._batch_size, self._size)
        idx = np.random.choice(self._size, size, replace=False)
        return (
            self._buffer[0][idx],
            self._buffer[1][idx],
            self._buffer[2][idx],
            self._buffer[3][idx],
        )
