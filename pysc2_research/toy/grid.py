import random

import gym
from gym.spaces import Discrete


def clamp(x, min_x, max_x):
    return min(max(x, min_x), max_x)


class Grid(gym.Env):
    max_steps = 120
    dx = [0, 1, 0, -1]
    dy = [1, 0, -1, 0]

    def __init__(self, w, h):
        self.shape = (w, h)

        self.steps = None
        self.position = None
        self.goal = None

        self.action_space = Discrete(4)

    def get_obs(self):
        return list(self.position) + list(self.goal)

    def step(self, action: int):
        self.steps += 1

        x, y = self.position
        x = clamp(x + Grid.dx[action], 0, self.shape[0] - 1)
        y = clamp(y + Grid.dy[action], 0, self.shape[1] - 1)

        self.position = (x, y)
        reward = 0

        if self.position == self.goal:
            reward = 1
            self.goal = get_location(self.shape, self.position)

        done = self.steps == Grid.max_steps

        return self.get_obs(), reward, done, {}

    def reset(self):
        self.position = get_location(self.shape, (-1, -1))
        self.goal = get_location(self.shape, self.position)
        self.steps = 0
        return self.get_obs()

    def render(self, mode='ansi'):
        w, h = self.shape
        table = [['.'] * h for _ in range(w)]
        x, y = self.position
        table[x][y] = '*'
        x, y = self.goal
        table[x][y] = 'G'
        print('\n'.join(''.join(row) for row in table)+'\n')


def get_location(shape, diff):
    w, h = shape
    while True:
        x, y = random.randint(0, w - 1), random.randint(0, h - 1)
        if (x, y) != diff:
            break
    return x, y
