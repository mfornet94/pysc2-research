"""
Update one set of parameters at a time.
"""

import numpy as np
import tensorflow as tf


def actor(x):
    with tf.variable_scope("actor", reuse=tf.AUTO_REUSE):
        a = tf.layers.dense(x, 2, use_bias=False, name='dense')
    return a


def critic(a):
    with tf.variable_scope("critic", reuse=tf.AUTO_REUSE):
        v = tf.layers.dense(a, 1, use_bias=False, name='dense')
    return v


x = tf.placeholder(dtype=tf.float32, shape=(None, 2))
t = tf.placeholder(dtype=tf.float32, shape=(None, 1))

h = actor(x)
v = critic(h)

loss = tf.losses.mean_squared_error(t, v)

critic_parameters = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "critic")
critic_opt = tf.train.GradientDescentOptimizer(learning_rate=.001)
critic_update = critic_opt.minimize(loss, var_list=critic_parameters)

actor_parameters = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "actor")
actor_opt = tf.train.GradientDescentOptimizer(learning_rate=.001)
actor_update = actor_opt.minimize(loss, var_list=actor_parameters)


def show_vars(session):
    for var in tf.all_variables():
        print(var.name)
        print(session.run(var))
        print()


x_train = np.random.random((10, 2))
y_train = np.random.random((10, 1))

with tf.Session() as s:
    s.run(tf.initializers.global_variables())

    show_vars(s)
    print("UPDATE CRITIC\n")
    s.run([critic_update], feed_dict={x: x_train, t: y_train})
    show_vars(s)
    print("UPDATE ACTOR\n")
    s.run(actor_update, feed_dict={x: x_train, t: y_train})
    show_vars(s)
