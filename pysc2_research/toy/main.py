from grid import Grid

discount = 0.99


def main():
    env = Grid(5, 5)

    for i in range(1):
        env.reset()
        env.render()

        done = False
        total_reward = 0

        while not done:
            action = env.action_space.sample()
            _, r, done, _ = env.step(action)
            env.render()
            total_reward += r

        print("reward:", total_reward)


if __name__ == '__main__':
    main()
