from itertools import product
from pprint import pprint

from grid import Grid, clamp

eps = 1e-12


def get_discrete_state_function(w, h):
    states = all_states(w, h)
    states_tuple = list(enumerate(map(lambda s: tuple(s), states)))

    states_index = dict(map(lambda args: (args[1], args[0]), states_tuple))

    def get(s):
        return states_index[tuple(s)]

    return get


def all_states(w, h):
    states = []
    for x, y in product(range(w), range(h)):
        for xg, yg in product(range(w), range(h)):
            if (x, y) != (xg, yg):
                states.append([x, y, xg, yg])
    return states


def all_states_actions(w, h):
    states = all_states(w, h)
    state_actions = list(product(states, range(4)))
    return state_actions


def all_diff(w, h, x, y):
    states = []
    for xg, yg in product(range(w), range(h)):
        if (x, y) != (xg, yg):
            states.append([x, y, xg, yg])
    return states


def build_transitions(w, h, policy):
    """
    policy: Array of the form:
        [
            [state, action, proba],
            ...
        ]
    """
    # state action probability reward new_state
    transitions = []

    goal_proba = 1. / (w * h - 1)

    for state, action, proba in policy:
        if proba is None or proba < eps:
            # Don't include probabilities which are 0
            continue

        x, y, xg, yg = state
        xn = x + Grid.dx[action]
        yn = y + Grid.dy[action]
        xn = clamp(xn, 0, w)
        yn = clamp(yn, 0, h)

        if (xn, yn) == (xg, yg):
            for ns in all_diff(w, h, xn, yn):
                transitions.append([state, action, proba * goal_proba, 1., ns])
        else:
            ns = [xn, yn, xg, yg]
            transitions.append([state, action, proba, 0, ns])

    return transitions


def build_random_policy(w, h):
    state_actions = all_states_actions(w, h)
    policy = []
    uniform = 1. / 4
    for s, a in state_actions:
        policy.append([s, a, uniform])
    return policy


def build_optimal_policy(w, h):
    pass


def test_build_policy():
    policy = build_random_policy(2, 2)
    pprint(policy)


def test_build_transitions():
    w, h = 2, 2
    policy = build_random_policy(w, h)
    transition = build_transitions(w, h, policy)
    pprint(transition)


def test_discrete_function_construction():
    func = get_discrete_state_function(2, 2)
    print(func([0, 0, 1, 1]))


if __name__ == '__main__':
    # test_build_transitions()
    test_discrete_function_construction()
