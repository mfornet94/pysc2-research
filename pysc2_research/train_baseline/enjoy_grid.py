import time

import gym
from baselines.a2c import a2c
from baselines.common.vec_env import DummyVecEnv

# noinspection PyUnresolvedReferences
from keras.activations import relu

import envs

complexity = 'Large'


def main():
    env = gym.make("ParametricGrid{}-v0".format(complexity))
    _env = DummyVecEnv([lambda: env])

    act = a2c.learn(
        network='mlp',
        env=_env,
        total_timesteps=0,
        load_path="nn/{}.pkl".format(complexity.lower()),
        num_layers=3,
        num_hidden=32,
        activation=relu
    )

    while True:
        obs, done = env.reset(), False
        episode_rew = 0
        while not done:
            env.render()
            action = act.step(obs[None])[0][0]
            obs, rew, done, _ = env.step(action)
            episode_rew += rew
            time.sleep(0.1)
        print("Episode reward", episode_rew)


if __name__ == '__main__':
    main()
