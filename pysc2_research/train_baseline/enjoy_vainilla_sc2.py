import time

import gym
from baselines.a2c import a2c
from baselines.common.vec_env import DummyVecEnv

# noinspection PyUnresolvedReferences
import envs

complexity = 'Large'


def main():
    env = gym.make("MoveToBeaconCoordinatesViz-v0")

    _env = DummyVecEnv([lambda: env])

    act = a2c.learn(
        network='mlp',
        env=_env,
        total_timesteps=0,
        load_path="nn/move_to_beacon.pkl",
        num_layers=3,
        num_hidden=32,
    )

    while True:
        obs, done = env.reset(), False
        episode_rew = 0
        while not done:
            env.render()
            action = act.step(obs[None])[0][0]
            obs, rew, done, _ = env.step(action)
            episode_rew += rew
            time.sleep(0.1)
        print("Episode reward", episode_rew)


if __name__ == '__main__':
    from absl import flags
    import sys

    FLAGS = flags.FLAGS
    FLAGS(sys.argv)

    main()
