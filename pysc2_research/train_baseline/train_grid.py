from functools import partial
from os import environ

import gym
from baselines.a2c import a2c
from baselines.common.vec_env import DummyVecEnv
from keras.activations import relu

# noinspection PyUnresolvedReferences
import envs

complexity = 'Large'


def main():
    environ['OPENAI_LOGDIR'] = './logs'

    env = DummyVecEnv([partial(gym.make, "ParametricGrid{}-v0".format(complexity))])

    act = a2c.learn(
        network='mlp',
        env=env,
        total_timesteps=100000,
        num_layers=3,
        num_hidden=32,
        activation=relu
    )
    print("Saving model to parametric_grid_{}.pkl".format(complexity.lower()))
    act.save("nn/{}.pkl".format(complexity.lower()))


if __name__ == '__main__':
    main()
