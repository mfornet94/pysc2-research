from functools import partial
from os import environ

import gym
from baselines.a2c import a2c
from baselines.common.vec_env import DummyVecEnv
from keras.activations import relu

# noinspection PyUnresolvedReferences
import pysc2_research.envs


def main(total_timesteps=100000, num_env=1, log_path='./logs-sc2', model_path='nn/move_to_beacon.pkl'):
    total_timesteps = int(total_timesteps)
    num_env = int(num_env)

    print("""
    total timesteps: {}
    num environments (a2c): {}
    log path: {}
    model path: {}
    """.format(total_timesteps, num_env, log_path, model_path))

    environ['OPENAI_LOGDIR'] = log_path

    env = DummyVecEnv([partial(gym.make, "MoveToBeaconCoordinates-v0") for _ in range(num_env)])

    act = a2c.learn(
        network='mlp',
        env=env,
        total_timesteps=total_timesteps,
        num_layers=3,
        num_hidden=32,
        activation=relu
    )

    act.save(model_path)


def get(args):
    dic = {}
    for key, value in zip(args[:-1], args[1:]):
        if key.startswith('--'):
            dic[key[2:]] = value
    return dic


if __name__ == '__main__':
    from absl import flags
    import sys

    FLAGS = flags.FLAGS
    FLAGS(sys.argv[:1])

    dic = get(sys.argv)

    main(**dic)
