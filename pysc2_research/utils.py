import json
from os.path import join

import numpy as np

from config import Configuration


def model_path(size, her):
    return join(Configuration.saved_models_path, 'weights-{}-{}.h5'.format(
        size, "her" if her else "noher"))


class SmoothValue:
    def __init__(self):
        self._value = None

    def update(self, x):
        if self._value is None:
            self._value = x
        else:
            self._value = .99 * self._value + .01 * x

    @property
    def value(self):
        return self._value


def load_config(path):
    with open(path) as f:
        config = json.load(f)
    return config


class State:
    def __init__(self, state, goal=None):
        self.state = state
        self.goal = goal


class LogSchedule:
    def __init__(self, init_value, final_value, steps):
        self.init_value = np.log(init_value)
        self.final_value = np.log(final_value)
        self.steps = steps

    def value(self, t):
        t = min(t, self.steps)
        alpha = t / self.steps
        x = (1. - alpha) * self.init_value + alpha * self.final_value
        return np.exp(x)
