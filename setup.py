import setuptools

setuptools.setup(
    name='pysc2_research',
    version='0.0.1',
    author='Roman Pozas, Marcelo Fornet',
    author_email='mfornet94@gmail.com',
    description='A small example package',
    url='www.google.com',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ]
)
